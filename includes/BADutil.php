<?PHP

/* * *********************** saihduero.es *********************** */
/* * ************************ Version: 3.0 ********************** */
/* DESCRIPCCI�N: Funciones globales utilizadas en todo el sitio Web */

/* * ***************** A�adir texto ************************* */

/// Carga un fichero de texto y lo prepara para traducir su contenido a html\\\
///$filename: Ruta del archivo que ser� cargado\\\

function readTxtFile($filename) {
    $fo = fopen($filename, 'r');
    if ($fo = fopen($filename, 'r')) {
        echo '<p></p><center><div id=text><table border = 0 width="90%" align="justify">';
        while (!feof($fo)) {
            $line = fgets($fo);
            $txt = bbCode($line);
            echo "<tr><td align='left'>$txt</td></tr>";
        }
        echo "</table></div></center>";
        fclose($fo);
    } else {
        echo "<br><b>Archivo $filename no encontrado</b>";
    }
}

/// Convierte las etiquetas en el fichero de texto en su equivalente html\\\
/// $txt: Texto que ser� convertido\\\
function bbCode($txt) {
    $txt = htmlentities($txt);
    $a = array(
        "/\[i\](.*?)\[\/i\]/is",
        "/\[b\](.*?)\[\/b\]/is",
        "/\[u\](.*?)\[\/u\]/is",
        "/\[table\](.*?)\[\/table\]/is",
        "/\[tr\](.*?)\[\/tr\]/is",
        "/\[td\](.*?)\[\/td\]/is",
        "/\[title\](.*?)\[\/title\]/is",
        "/\[subtitle\](.*?)\[\/subtitle\]/is",
        "/\[bloq\](.*?)\[\/bloq\]/is",
        "/\[center\](.*?)\[\/center\]/is",
        "/\[img\](.*?)\[\/img\]/is",
        "/\[imgCom\](.*?)\[\/imgCom\]/is",
        "/\[imgC\](.*?)\[\/imgC\]/is",
        "/\[url=(.*?)\](.*?)\[\/url\]/is",
        "/\[linkM=(.*?)\](.*?)\[\/linkM\]/is",
        "/\[linkP=(.*?)\](.*?)\[\/linkP\]/is",
        "/\[popup=(.*?)\](.*?)\[\/popup\]/is",
        "/\[br]/is",
        "/\[es]/is",
    );
    $b = array(
        "<i>$1</i>",
        "<b>$1</b>",
        "<u>$1</u>",
        "<table align=\"left\" bgcolor=\"#dbe5f0\" >$1</table>",
        "<tr>$1</tr>",
        "<td>$1</td>",
        "<table align=\"left\"><tr><td class=\"titlePHP\">$1</tr></tr></table>",
        "<table align=\"left\"><tr><td class=\"subtitle\">$1</tr></tr></table>",
        "<blockquote>$1</blockquote>",
        "<center>$1</center>",
        "<img border=0 src=\"$1\" />",
        "<table><tr><td align=\"center\"><img border=0 src=\"$1\" /></tr></tr></table>",
        "<img align=\"center\" border=0 src=\"$1\" />",
        "<a href=\"$1\" target=\"_blank\">$2</a>",
        "<a href=\"$1\">$2</a>",
        "<a name=\"$1\">$2</a>",
        "<a href=\"#\" onclick = \"ventanaNueva($1)\" >$2</a>",
        "<br /><br />",
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;",
    );
    $txt = preg_replace($a, $b, $txt);
    $txt = nl2br($txt);
    return $txt;
}

///Devuelve un array con los ficheros que se encuentran dentro de un directorio de la carpeta pdf dentro de informes.
///$signa: QU,TE,NI,... C�digo identificador de cada tipo de se�al.
///$rtu: c�digo rtu de la estaci�n.
function getFiles($signal, $rtu) {
    if (strlen($rtu) == 6)
        $path = './pdf/' . $rtu . '/' . $signal . '/';
    else if (strlen($rtu) == 5)
        $path = './pdf/' . substr($rtu, 0, 2) . '/' . $rtu . '/' . $signal . '/';
    else
        $path = './pdf/' . $rtu . '/';
    if (file_exists($path)) {
        try {
            $dh = opendir($path);
            //comprobar si existen
            while (false !== ($nombre_archivo = readdir($dh))) {
                $files[] = $nombre_archivo;
            }
            return $files;
        } catch (GlobalError $e) {
            return $e->getMessage();
        }
    }
}

// Recorre todos los archivos que se encuentran dentro de la carpeta de informes de la estacion y devuelve un array con
// el nombre del fichero, el dia, el mes y el a�o.
// $signa: QU,TE,NI,... C�digo identificador de cada tipo de se�al.
// $rtu: c�digo rtu de la estaci�n.

function getDates($signal, $rtu) {
    $files = getFiles($signal, $rtu);
    $filename_header = strlen($rtu) + 1;
    if (strlen($signal) > 0) {
        $filename_header = strlen($rtu) + strlen($signal) + 2;
    }
    for ($i = 0; $i < count($files); $i++) {
        $filesDate[$i][0] = $files[$i];
        $filesDate[$i][1] = substr($files[$i], $filename_header + 0, 2);
        $filesDate[$i][2] = substr($files[$i], $filename_header + 2, 2);
        $filesDate[$i][3] = substr($files[$i], $filename_header + 4, 4);
    }
    return $filesDate;
}

function linkReport($signal, $rtu) {
    $files = getDates($signal, $rtu);
    $date = getActual($files);
    return $rtu . '_' . $signal . '_' . $date . '.pdf';
    //$numFiles = count($files)-1;
    //return $files[$numFiles][0];
}

// A partir del array creado en getDatea coge el campo de los meses para devolver un array con los meses de los que 
// se tienen informes.
// $signa: QU,TE,NI,... C�digo identificador de cada tipo de se�al.
// $rtu: c�digo rtu de la estaci�n.

function printHis($signal, $dates) {
    $files = getDates($signal, $dates);
    for ($i = 0; $i < count($files); $i++) {
        $months[$i] = $files[$i][2];
    }
    echo '<br><br>';
    for ($i = 0; $i < count($months); $i++) {
        if ($months[$i] == '01') {
            $his[0] = 'Enero';
        } else if ($months[$i] == '02') {
            $his[1] = 'Febrero';
        } else if ($months[$i] == '03') {
            $his[2] = 'Marzo';
        } else if ($months[$i] == '04') {
            $his[3] = 'Abril';
        } else if ($months[$i] == '05') {
            $his[4] = 'Mayo';
        } else if ($months[$i] == '06') {
            $his[5] = 'Junio';
        } else if ($months[$i] == '07') {
            $his[6] = 'Julio';
        } else if ($months[$i] == '08') {
            $his[7] = 'Agosto';
        } else if ($months[$i] == '09') {
            $his[8] = 'Septiembre';
        } else if ($months[$i] == '10') {
            $his[9] = 'Octubre';
        } else if ($months[$i] == '11') {
            $his[10] = 'Noviembre';
        } else if ($months[$i] == '12') {
            $his[11] = 'Diciembre';
        }
    }
    return $his;
}

function getActual($files) {
    //$year = 0;
    //$month = 0;
    //$day = 0;
    $totaldate = 0;
    for ($i = 0; $i < count($files); $i++) {
        $thisdate = $files[$i][3] * 10000 + $files[$i][2] * 100 + $files[$i][1];
        if ($thisdate > $totaldate) {
            $totaldate = $thisdate;
            $year = $files[$i][3];
            $month = $files[$i][2];
            $day = $files[$i][1];
        }
    }
    return $day . $month . $year;
}

function getM($date) {
    switch ($date) {
        case $date == 1;
            return 'Enero';
            break;
        case $date == 2;
            return 'Febrero';
            break;
        case $date == 3;
            return 'Marzo';
            break;
        case $date == 4;
            return 'Abril';
            break;
        case $date == 5;
            return 'Mayo';
            break;
        case $date == 6;
            return 'Junio';
            break;
        case $date == 7;
            return 'Julio';
            break;
        case $date == 8;
            return 'Agosto';
            break;
        case $date == 9;
            return 'Septiembre';
            break;
        case $date == 10;
            return 'Octubre';
            break;
        case $date == 11;
            return 'Noviembre';
            break;
        case $date == 12;
            return 'Diciembre';
            break;
    }
}

function round_number($value = "", $precision = 2) {
    $rounded = round($value, $precision);
    $formated = number_format($rounded, $precision, '.', '');
    return $formated;
}

function format_number($value = "", $precision = 2) {
    $value = str_replace(',', '', $value);
    return (float) $value;
}

function check_date($date = "", $findme = "") {
    $pos = strpos($date, $findme);
    if ($pos !== FALSE) {
        return FALSE;
    } else {
        return TRUE;
    }
}

function check_date_week($date = '') {
    $explode = explode(' ', $date);
    $date_string = explode('-', $explode[0]);

    $monthArr = array('Ene' => '01', 'Feb' => '02', 'Mar' => '03', 'Abr' => '04', 'May' => '05',
        'Jun' => '06', 'Jul' => '07',
        'Ago' => '08', 'Sep' => '09', 'Oct' => '10', 'Nov' => '11', 'Dic' => '12');

    $day = $date_string[0];
    $mon = ucfirst($date_string[1]);
    $month = $monthArr[$mon];
    $year = $date_string[2];

    $formated_date = "$month/$day/$year";
    $date = strtotime($formated_date);

    $week_before = $date - (60 * 60 * 24 * 7); // Una semana antes

    if ($date > $week_before) {
        return TRUE;
    } else {
        return FALSE;
    }


    return TRUE;
}

function alarm_button($value = 0) {
    switch ($value) {
        case 0:
            $alarm = '<img src = "./assets/img/bullet_yellow.png" width = "24" height = "24" alt = "' . $value . '"/>';
            break;
        case 1:
            $alarm = '<img src = "./assets/img/bullet_green.png" width = "24" height = "24" alt = "' . $value . '"/>';
            break;
        case 2:
            $alarm = '<img src = "./assets/img/bullet_red.png" width = "24" height = "24" alt = "' . $value . '"/>';
            break;
        default:
            $alarm = '<img src = "./assets/img/bullet_white.png" width = "24" height = "24" alt = "' . $value . '"/>';
            break;
    }
    return $alarm;
}

function sortArrayByArray($array, $orderArray) {
    $ordered = array();
    foreach ($orderArray as $tag) {

        $key = explode('/', $tag);
        if (count($key) === 1) {
            $key = $key[0];
            if (array_key_exists($key, $array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
            }
        }
    }
    return $ordered + $array;
}

function my_sort_desc($obj1, $obj2) {

    $date1 = isset($obj1['medicion']) ? $obj1['medicion'] : $obj1[0];
    $date2 = isset($obj2['medicion']) ? $obj2['medicion'] : $obj2[0];

    $string_to_time1 = strtotime($date1);
    $string_to_time2 = strtotime($date2);

    if ($string_to_time1 == $string_to_time2) {
        return 0; // equal to
    } else if ($string_to_time1 > $string_to_time2) {
        return 0; // equal to) {
        return -1; // less than
    } else {
        return 1; // greater than
    }
}

function alarmas2array($xml) {
    $arr = array();
    foreach ($xml as $element) {
        $tipo = (string) $element->attributes()->tipo;

        foreach ($element as $data) {
            $estacion = (string) $data->attributes()->estacion;

            foreach ($data as $variables) {
                $variable = (string) $variables->attributes()->variable;
                $arr[$tipo][$estacion][$variable]['Superior'] = (string) $variables->attributes()->Superior;
                $arr[$tipo][$estacion][$variable]['Inferior'] = (string) $variables->attributes()->Inferior;
                $arr[$tipo][$estacion][$variable]['Estado'] = (string) $variables->attributes()->Estado;
            }
        }
    }

    return $arr;
}

?>
