<?php

class db_pgsql {

    public $config;
    private static $instance;

    public function __construct() {
        $this->config = parse_ini_file('config.ini', true);
    }

    public static function singleton() {
        if (!isset(self::$instance)) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    function connect() {
        error_reporting(0); // turn on errors display
        $connection_string = "host=" . $this->config['db_server'] . " port=" . $this->config['db_port'] .
                " dbname=" . $this->config['db_schema'] . " user=" . $this->config['db_user'] .
                " password=" . $this->config['db_pass'];

        $this->conexion = pg_connect($connection_string)
                or die($this->config['db_error_conexion']);
        error_reporting(-1);  // turn on errors display
        return $this->conexion;
    }

    //hacer consulta
    function query($query) {
        if ($query) { // make sure that previous query are correct
            error_reporting(0); // turn on errors display
            $query_object = pg_query($this->conexion, $query);
            error_reporting(-1);  // turn on errors display
            if ($query_object) {
                return $query_object;
            } else {
                return $this->postgres_errors();
            }
        } else {
            return FALSE;
        }
    }

    function get_result_array($result) {
        if ($result) {
            $data_array = pg_fetch_all($result);
            if ($data_array) {
                return $data_array;
            } else {
                return array();
            }
        } else {
            return array();
        }
    }

    //cerrar conexion
    function close() {
        pg_close($this->conexion);
    }

    function postgres_errors() {
        $base_dir = str_replace('\\\\', '/', realpath(dirname(__FILE__))) . '/';
        //Get real path for root dir ---linux and windows
        $fecha = date("Y-m-d h:i:s");
        $error = pg_last_error($this->conexion);
        error_log("$fecha La consulta falló con el siguiente error =>  \n $error \n"
                , 3, $base_dir . "logs/database.log");
        return FALSE;
    }

}

?>