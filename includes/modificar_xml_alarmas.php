<?php
error_reporting(E_ERROR);  	
$estacion_array = explode('/', $cod_variable);
$estacion = isset($estacion_array[0]) ? $estacion_array[0] : '';

if ($mod === 'borrar') {
    
} else {
    $query = "//node[@label='" . $estacion . "']";
    $xml = simplexml_load_file('../xmls/datos_estaciones.xml');
    $result = $xml->xpath($query);
    $xml_alarmas = simplexml_load_file('../xmls/alarmas.xml');
    $result_alarmas = $xml_alarmas->xpath($query);

    if (isset($result[0]) AND empty($result_alarmas[0])) {
        $estacion_xml = $result[0]->asXml();
        $sxe = new SimpleXMLElement($estacion_xml);

        $nodo = $xml_alarmas->addChild('node');

        $label = (string) $sxe->attributes()->label;
        $nodo->addAttribute('label', $label);

        $descripcion = (string) $sxe->attributes()->descripcion;
        $nodo->addAttribute('descripcion', $descripcion);

        $zona = (string) $sxe->attributes()->zona;
        $nodo->addAttribute('zona', $zona);

        $tipo = (string) $sxe->attributes()->tipo;
        $nodo->addAttribute('tipo', $tipo);

        $tipo2 = (string) $sxe->attributes()->tipo2;
        $nodo->addAttribute('tipo2', $tipo2);

        $ttmm = (string) $sxe->attributes()->ttmm;
        $nodo->addAttribute('ttmm', $ttmm);
        $subcuenca = (string) $sxe->attributes()->subcuenca;
        $nodo->addAttribute('subcuenca', $subcuenca);
        $provincia = (string) $sxe->attributes()->provincia;
        $nodo->addAttribute('provincia', $provincia);
        $cy = (string) $sxe->attributes()->cy;
        $nodo->addAttribute('cy', $cy);
        $cx = (string) $sxe->attributes()->cx;
        $nodo->addAttribute('cx', $cx);

        foreach ($sxe as $tipo) {
            $grupo = (string) $tipo->attributes()->grupo;
            if ($grupo === 'Estandar') {
                $nodo_tipo = $nodo->addChild('node');
                $nodo_tipo->addAttribute('grupo', $grupo);
                foreach ($tipo as $atributos) {
                    $sub_nodo = $nodo_tipo->addChild('node'); //Creamos multiples nodos
                    if (is_object($sub_nodo)) {
                        $medida = (string) $atributos->attributes()->medida;
                        $unidad = (string) $atributos->attributes()->unidad;
                        $llamada = (string) $atributos->attributes()->llamada;
                        $representativa = (string) $atributos->attributes()->representativa;
                        $puntohis = (string) $atributos->attributes()->puntohis;
                        $columnahis = (string) $atributos->attributes()->columnahis;

                        $sub_nodo->addAttribute('medida', $medida);
                        $sub_nodo->addAttribute('unidad', $unidad);
                        $sub_nodo->addAttribute('llamada', $llamada);
                        $sub_nodo->addAttribute('representativa', $representativa);
                        $sub_nodo->addAttribute('puntohis', $puntohis);
                        $sub_nodo->addAttribute('columnahis', $columnahis);
                    }
                }
            }
        }
        $xml_alarmas->asXml('../xmls/alarmas.xml');
    }
}
?>