<?php

/* * *******************************************************************
  class.email.php
 * ******************************************************************** */

class Email {

    var $email;
    var $name;
    private $info;

    function __construct() {
        $this->config = parse_ini_file('config.ini', true);

        $this->email = $this->config['email'];
        $this->name = $this->config['name'];

        $this->info['smtp_host'] = $this->config['smtp_host'];
        $this->info['userpass'] = $this->config['userpass'];
        $this->info['userid'] = $this->config['userid'];

        $this->info['mail_protocol'] = $this->config['mail_protocol'];
        $this->info['smtp_port'] = $this->config['smtp_port'];
        $this->info['smtp_auth'] = $this->config['smtp_auth'];
    }

    function getEmail() {
        return $this->email;
    }

    function getName() {
        return $this->name;
    }

    function getSMTPInfo() {
        $info = array('host' => $this->info['smtp_host'],
            'port' => $this->info['smtp_port'],
            'auth' => $this->info['smtp_auth'],
            'username' => $this->info['userid'],
            'password' => $this->info['userpass'],
        );
        return $info;
    }

    function send($to, $subject, $message, $cc = '') {

        $smtp = $this->getSMTPInfo();
		
        if ($smtp) { //Send via SMTP
            //Get the goodies
			//error_reporting(E_ALL);  		
	//require_once ('pear/Mail.php');// PEAR Mail package
	//require_once ('pear/Mail/mime.php'); // PEAR Mail_Mime packge

require_once 'Mail.php';
require_once 'Mail/mime.php';


          
        //do some cleanup
	
            $recipients = preg_replace("/(\r\n|\r|\n)/s", '', trim($to . ', ' . $cc)); // Añadimos los correos en cc
            $subject = stripslashes(preg_replace("/(\r\n|\r|\n)/s", '', trim($subject)));
            $body = stripslashes(preg_replace("/(\r\n|\r)/s", "\n", trim($message)));
            $fromname = $this->getName();
            $from = sprintf('"%s"<%s>', ($fromname ? $fromname : $this->getEmail()), $this->getEmail());

            $headers = array(
                'From' => $from,
                'To' => $to,
                'CC' => $cc,
                'Subject' => $subject,
                'Date' => date("D , d M Y H:i:s O"),
                'Message-ID' => '<' . $this->randCode(6) . '' . time() . '-' . $this->getEmail() . '>',
                'X-Mailer' => 'Sveltez v 1.6',
                'Content-Type' => 'text/html; charset="UTF-8"'
            );
            $eol = "\n"; //crlf
            $mime = new Mail_mime($eol);

            $mime->setHTMLBody($body);   //  $mime->setTXTBody($body);

            $options = array('head_encoding' => 'quoted-printable',
                'text_encoding' => 'quoted-printable',
                'html_encoding' => 'base64',
                'html_charset' => 'utf-8',
                'text_charset' => 'utf-8');
            //encode the body
            $body = $mime->get($options);
            //encode the headers.
            $headers = $mime->headers($headers);

            $mail = Mail::factory('smtp', array(
                        'host' => $smtp['host'],
                        'port' => $smtp['port'],
                        'auth' => $smtp['auth'] ? true : false,
                        'username' => $smtp['username'],
                        'password' => $smtp['password'],
                        'timeout' => 20,
                        'debug' => false,
                    ));
			
            $result = $mail->send($recipients, $headers, $body);
         if (!PEAR::isError($result)) {		
                return true;
            } else {
			// echo var_dump($mail);
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    function randCode($len = 8) {
        return substr(strtoupper(base_convert(microtime(), 10, 16)), 0, $len);
    }

}

?>
