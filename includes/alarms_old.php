<?php

include_once 'db_pgsql.php';
include_once 'util.php';
$mod = isset($_GET['mod']) ? $_GET['mod'] : 'none';

if ($mod === 'borrar') {
    $db_pgsql = db_pgsql::singleton();
    $db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

    $indice_alarma = isset($_GET['indice_alarma']) ? $_GET['indice_alarma'] : '';
    $cod_variable = isset($_GET['cod_variable']) ? rawurldecode($_GET['cod_variable']) : '';
    $query_delete = "DELETE FROM tbl_alarmas WHERE indice_alarma='$indice_alarma' AND cod_variable = '$cod_variable'; COMMIT;";
    $success_query = $db_pgsql->query($query_delete);
    $success = $success_query === FALSE ? FALSE : TRUE;

    if ($success) {
        $info = 'Alarma borrada correctamente';
        include_once 'modificar_xml_alarmas.php';
    } else {
        $info = 'La alrma no ha sido borrada!';
    }

    $json = array("success" => $success, "info" => $info);
    print_r(json_encode($json));
} else {
    $info = "";
    $success = TRUE;
    $errors = array();
    $validate = array(
        "empty" => array('cod_variable', 'tipo', 'umbral', 'correo_para'),
        "number" => array('umbral',),
        "mail" => array('correo_para', 'correo_cc'),
    );

    foreach ($_POST as $key => $post) {
        if (in_array($key, $validate['empty'])) {// validamos que no está vacío.
            $errors[$key]['name'] = $key;
            $errors[$key]['empty'] = empty($post) ? 0 : 1;
            $errors[$key]['value'] = $post;
            if (empty($post) AND $success) {
                $success = FALSE;
                $info .= 'Todos los campos en Rojo son obligatorios';
            }
        }

        if (in_array($key, $validate['number'])) { // validamos que es un numero
            $errors[$key]['name'] = $key;
            $errors[$key]['empty'] = empty($post) ? 0 : 1;
            $errors[$key]['value'] = $post;
            $number = str_replace(',', '.', $post);
            if (!is_numeric($number) AND $success) {
                $success = FALSE;
                $info .= ucfirst($key) . ' debe ser un número';
            }
        }
        if (!empty($post)) {
            if (in_array($key, $validate['mail'])) { // validamos que es un numero
                $errors[$key]['name'] = $key;
                $errors[$key]['empty'] = empty($post) ? 0 : 1;
                $errors[$key]['value'] = $post;
               // $array_mails = explode(';', $post);
				$array_mails = preg_split('/;|,/', $post);  
				
                foreach ($array_mails as $mail) {
                    if (!filter_var($mail, FILTER_VALIDATE_EMAIL) AND $success) {
                        $success = FALSE;
                        $info .= ucfirst($key) . ' debe ser un correo válido, debe separarse por , o ;';
                        break 2;
                    }
                }
            }
        }
    }

    if ($success) {
        $tipo_estacion = isset($_POST['tipo_estacion']) ? rawurldecode($_POST['tipo_estacion']) : '';
        $cod_estacion = isset($_POST['cod_variable'][0]) ? rawurldecode($_POST['cod_variable'][0]) : '';
        $tipo = isset($_POST['tipo']) ? rawurldecode($_POST['tipo']) : '';

        $umbral = isset($_POST['umbral']) ? format_number((str_replace(',', '.', $_POST['umbral'])), 2) : '';
        $habilitada = isset($_POST['habilitada']) ? 't' : 'f';

        $correo_cc = isset($_POST['correo_cc']) ? $_POST['correo_cc'] : '';
        $correo_para = isset($_POST['correo_para']) ? $_POST['correo_para'] : '';

        $usuario = isset($_POST['usuario']) ? $_POST['usuario'] : '';

        $db_pgsql = db_pgsql::singleton();
        $db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

        if ($mod === 'nueva') {
            $cod_variable = isset($_POST['cod_variable']) ? rawurldecode($_POST['cod_variable'][0] . '/' . $_POST['cod_variable'][1]) : '';
            $cod_unidad = isset($_POST['cod_variable'][1]) ? rawurldecode($_POST['cod_variable'][1]) : '';

            $query = "SELECT indice_alarma FROM tbl_alarmas
                        WHERE cod_variable='$cod_variable'  AND usuario ='$usuario' " . //AND tipo='$tipo'
                    "ORDER BY indice_alarma asc";
            $result_last_index = $db_pgsql->query($query);
            $indice_alarma = $db_pgsql->get_result_array($result_last_index);
            if ($indice_alarma !== FALSE) {
                if (!empty($indice_alarma)) {
                    $indice_alarma = end($indice_alarma);
                    $indice_alarma = $indice_alarma['indice_alarma'] + 1;
                } else {
                    $indice_alarma = 0;
                }

                $query = "INSERT INTO tbl_alarmas (cod_variable, tipo, 
                        umbral, indice_alarma, cod_estacion, usuario, habilitada, correo_para, correo_cc, 
                        tipo_estacion, enviada, umbral_activado, cod_unidad)
                VALUES ('$cod_variable','$tipo','$umbral','$indice_alarma', '$cod_estacion',
                        '$usuario', '$habilitada', '$correo_para', '$correo_cc', '$tipo_estacion', '0', '', '$cod_unidad');            
                            COMMIT;";
                $success_query = $db_pgsql->query($query);
                $success = $success_query === FALSE ? FALSE : TRUE;
                $info = $success ? "Alarma creada correctamente " : "Error al crear la alarma";

                if ($success)
                    include_once 'modificar_xml_alarmas.php';
            } else {
                $info = "Error al crear la alarma";
            }
        } elseif ($mod === 'editar') {
            $cod_variable = isset($_POST['cod_variable']) ? rawurldecode($_POST['cod_variable']) : '';
            $cod_unidad_array = isset($_POST['cod_variable']) ? explode('/', $_POST['cod_variable']) : '';
            $cod_unidad = isset($cod_unidad_array[1]) ? $cod_unidad_array[1] : '';
            $indice_alarma = isset($_POST['indice_alarma']) ? $_POST['indice_alarma'] : '';
            $query = "UPDATE tbl_alarmas SET tipo='$tipo', 
                                umbral='$umbral', habilitada='$habilitada', correo_para ='$correo_para', correo_cc='$correo_cc',
                                    cod_unidad='$cod_unidad'
                              WHERE cod_variable='$cod_variable' AND indice_alarma='$indice_alarma'
                                    AND usuario ='$usuario'; COMMIT;";
            $success_query = $db_pgsql->query($query);
            $success = $success_query === FALSE ? FALSE : TRUE;
            $info = $success ? "Alarma actualizada correctamente " : "Error al modificar la alarma";
        }

        $modal = $_POST['modal'];
        $errors = "";
    } else {
        $modal = ""; // NO Cerramos el Modal
        $errors = $errors;
        $redirect = FALSE;
    }
    $redirect = $_SERVER['HTTP_REFERER'];
    $json = array(
        "success" => $success, //Insertado correctamente
        "info" => $info,
        "modal" => $modal, // Cerramos el Modal
        "errors" => $errors, // ID de latabla donde agregaremos la nueva ROW
        "redirect" => $redirect,
    );
    print_r(json_encode($json));
}
?>