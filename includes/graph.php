<?php

/**
 * Description of graph
 *
 * @author francesc.ruiz
 */
include_once 'util.php';

class graph {

    public $url;
    public $station;
    public $station_type;

    function __construct($url = "", $station_type = "") {
        $this->url = $url;
        $this->station_type = $station_type;
    }

    function call($station = "", $parameter = "") {
        $milliseconds = round(microtime(true) * 1000);
        $variables = "stationType=" . $this->station_type . "&stationID=" . $station
                . "&variableID=$station/$parameter";
        //  $variables .= "&optionalEndDate=2013-07-11 23:59:59";  // parámetro extra
        $set_hist = $this->url . "setHistoricalParameters?";

        $xml_dummy = $this->url . "config-graf.xml?nocache=" . $milliseconds;
        $csv_values = $this->url . "values.csv?nocache=" . $milliseconds;

        $output = array();

        $sess_id = $_COOKIE['PHPSESSID']; //session_id();
        // guardamos la cookie de sesión para poder mantenerla viva entre llamadas al Webservice.
        $strCookie = 'PHPSESSID=' . $sess_id . '; path=/';
        session_write_close();

        $ch = curl_init(); // Iniciamos la llamada.
        curl_setopt($ch, CURLOPT_URL, $set_hist); // set url
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        // Send post values
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $variables);

        // make return output data TRUE
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        // Follow any 302 header
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);

        // keep session alive between CALLS
        curl_setopt($ch, CURLOPT_COOKIE, $strCookie);
        curl_setopt($ch, CURLOPT_COOKIEFILE, $strCookie);
        curl_setopt($ch, CURLOPT_COOKIEJAR, $strCookie);

        $output['set'] = curl_exec($ch); // guardamos la información recibida

        curl_setopt($ch, CURLOPT_URL, $xml_dummy);  // reset url

        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        $output['xml'] = curl_exec($ch); // guardamos la información recibida

        curl_setopt($ch, CURLOPT_URL, $csv_values); // reset url
        $output['csv'] = curl_exec($ch); // guardamos la información recibida

        curl_close($ch); // cerramos la llamada

        $pattern = '<h1>Error: 500</h1>';
        if ((strpos($output['csv'], $pattern) === false)) { // si ha funcionado guardamos el csv
            return $this->save_csv($output['csv'], "./data/$station.$parameter" . ".csv", 7);
        } else {
            // Ha habido un error
            $fecha = date("Y-m-d h:i:s");
            $base_dir = str_replace('\\\\', '/', realpath(dirname(__FILE__))) . '/';
            error_log("\n" . $fecha . " Error al conectarse al Webservice: \n" .
                    $output['csv'] . "\n \n", 3, $base_dir . "logs/RTAPiWS.log");
        }
        return $output['csv']; // output con los datos
    }

    function read_file($station = "", $parameter = "") { // Comprobamos 
        $path = "./data/$station.$parameter" . ".csv";

        if (file_exists($path)) { // Comprobamos si hemos creado el CSV
            $time_1 = filemtime($path);
            //$date_1 = date("d m Y H:i:s.", $time_1);
            $time_2 = strtotime("-15 minutes", strtotime('now'));
            // $date_2 = date("d m Y H:i:s.", $time_2);
            if ($time_1 > $time_2) { // Comprobamos que el CSV no tenga más de 15min.
                return file_get_contents($path); // Si ya lo tenemos lo parseamos!
            } else {
                return $this->call($station, $parameter); // LLamamos de nuevo al webservices
            }
        } else {
            return $this->call($station, $parameter); // LLamamos de nuevo al webservices
        }
    }

    function parse_csv($data = "") { // convertimos el CSV En un array.
        $return_array = array();
        $return_array['data'] = array();

        // variables de control para la gráfica MAX i MIN
        $return_array['variable_1']['min'] = 100000;
        $return_array['variable_1']['max'] = 0;

        $return_array['variable_2']['min'] = 100000;
        $return_array['variable_2']['max'] = 0;

        $parse_array = $this->mount_array($data, '7');
        if (!empty($parse_array)) {
            $length_1 = count($parse_array['variable_1']);
            $length_2 = count($parse_array['variable_2']);
            $array = $length_1 > $length_2 ? $length_1 : $length_2;


            if (!empty($data['variable_1']) AND !empty($data['variable_2'])) {

                for ($index = 0; $index < $array; $index++) {
                    //  if (isset($parse_array['variable_1'][$index]) AND isset($parse_array['variable_2'][$index])) {

                    $date_variable_1 = isset($parse_array['variable_1'][$index][0]) ? strtotime($parse_array['variable_1'][$index][0]) : 'variable_1';
                    $date_variable_2 = isset($parse_array['variable_2'][$index][0]) ? strtotime($parse_array['variable_2'][$index][0]) : 'variable_2';

                    if ($date_variable_1 === $date_variable_2) {
                        $return_array['data'][] = array(
                            $parse_array['variable_1'][$index][0],
                            format_number($parse_array['variable_1'][$index][1]),
                            format_number($parse_array['variable_2'][$index][1]),
                        );
                        $return_array['table_1'][] = array(
                            $parse_array['variable_1'][$index][0],
                            format_number($parse_array['variable_1'][$index][1]),
                        );
                        $return_array['table_2'][] = array(
                            $parse_array['variable_2'][$index][0],
                            format_number($parse_array['variable_2'][$index][1]),
                        );
                    } elseif ($date_variable_1 != $date_variable_2) {
                        if ($date_variable_1 !== 'variable_1') {
                            $return_array['data'][] = array(
                                $parse_array['variable_1'][$index][0],
                                format_number($parse_array['variable_1'][$index][1]), '',
                            );
                            $return_array['table_1'][] = array(
                                $parse_array['variable_1'][$index][0],
                                format_number($parse_array['variable_1'][$index][1]),
                            );
                        }
                        if ($date_variable_2 !== 'variable_2') {
                            $return_array['data'][] = array(
                                $parse_array['variable_2'][$index][0], '',
                                format_number($parse_array['variable_2'][$index][1]),
                            );
                            $return_array['table_2'][] = array(
                                $parse_array['variable_2'][$index][0],
                                format_number($parse_array['variable_2'][$index][1]),
                            );
                        }
                    }
                    if (isset($parse_array['variable_1'][$index])) {
                        if ($return_array['variable_1']['max'] < $parse_array['variable_1'][$index][1]) {
                            $return_array['variable_1']['max'] = format_number($parse_array['variable_1'][$index][1]);
                        }
                        if ($return_array['variable_1']['min'] > $parse_array['variable_1'][$index][1]) {
                            $return_array['variable_1']['min'] = format_number($parse_array['variable_1'][$index][1]);
                        }
                    }
                    if (isset($parse_array['variable_2'][$index])) {
                        if ($return_array['variable_2']['max'] < $parse_array['variable_2'][$index][1]) {
                            $return_array['variable_2']['max'] = format_number($parse_array['variable_2'][$index][1]);
                        }
                        if ($return_array['variable_2']['min'] > $parse_array['variable_2'][$index][1]) {
                            $return_array['variable_2']['min'] = format_number($parse_array['variable_2'][$index][1]);
                        }
                    }

                    //}
                }
            } else if (!empty($data['variable_1'])) {
                for ($index = 0; $index < $array; $index++) {
                    $return_array['data'][] = array(
                        $parse_array['variable_1'][$index][0],
                        format_number($parse_array['variable_1'][$index][1]),
                    );
                    if ($return_array['variable_1']['max'] < $parse_array['variable_1'][$index][1]) {
                        $return_array['variable_1']['max'] = format_number($parse_array['variable_1'][$index][1]);
                    }
                    if ($return_array['variable_1']['min'] > $parse_array['variable_1'][$index][1]) {
                        $return_array['variable_1']['min'] = format_number($parse_array['variable_1'][$index][1]);
                    }
                }
            }
        }
//        $count = count($return_array['data']);
        return $return_array;
    }

    function mount_array($data = array()) {// Leemos el CSV y parseamos en un array
        $return_array = array('variable_1' => array(), 'variable_2' => array());
        //$date_less = strtotime("-$days day", strtotime('now'));

        if (is_string($data['variable_1'])) {
            $variable_1 = explode("\n", $data['variable_1']);

            foreach ($variable_1 as $key => $value) {
                $val = explode('",', $value);              // rompemos el CSV cuidado con los miles que llevan coma.
                if (isset($val[0]) AND isset($val[1])) {
                    $str_to_time = str_replace(array('"'), "", $val[0]);  //quitamos " para limpiar datos
                    $value = str_replace(array('"'), "", $val[1]);

                    if (is_numeric($value)) {
                        $format = number_format(round($value, 2), 2);
                    } else {
                        $format = $value;
                    }
                    $return_array['variable_1'][] = array($str_to_time, $format);
                }
            }
        }

        if (is_string($data['variable_2'])) {
            $variable_2 = explode("\n", $data['variable_2']);
            foreach ($variable_2 as $key => $value) {
                $val = explode('",', $value); // rompemos el CSV cuidado con los miles que llevan coma.
                if (isset($val[0]) AND isset($val[1])) {
                    $str_to_time = str_replace(array('"'), "", $val[0]); //quitamos " para limpiar datos
                    $value = str_replace(array('"'), "", $val[1]);
                    if (is_numeric($value)) {
                        $format = number_format(round($value, 2), 2);
                    } else {
                        $format = $value;
                    }
                    $return_array['variable_2'][] = array($str_to_time, $format);
                }
            }
        }
        return $return_array;
    }

    // Buscamos fechas iguales, si no estuvieran iriamos pasando hasta encontrar concordancia.
    function return_same_date($index1, $index2, $array, $variable_1, $variable_2) {
        if (isset($variable_1[$index1]) AND isset($variable_2[$index2])) {
            if ($index1 < $array OR $index2 < $array) {
                $date_variable_1 = strtotime($variable_1[$index1][0]);
                $date_variable_2 = strtotime($variable_2[$index2][0]);
                if ($date_variable_1 == $date_variable_2) {
                    return array($index1, $index2);
                } else if ($date_variable_1 < $date_variable_2) {
                    $index1++;
                    return $this->return_same_date($index1, $index2, $array, $variable_1, $variable_2);
                } else if ($date_variable_1 > $date_variable_2) {
                    $index2++;
                    return $this->return_same_date($index1, $index2, $array, $variable_1, $variable_2);
                }
            }
        }
    }

    function save_csv($data, $path = "", $days = '7') { // Leemos el CSV y parseamos en un array
        $return_array = Array();
        $date_less = strtotime("-$days day", strtotime('now'));

        if (is_string($data)) {
            $variable = explode("\n", $data);

            foreach ($variable as $value) {
                $val = explode(',', $value);

                $date = strtotime($val[0]);
                if ($date > $date_less) {
                    $value = (float) $val[1];
                    if (is_numeric((float) $val[1])) {
                        $format = number_format(round((float) $val[1], 2), 2);
                    } else {
                        $format = $val[1];
                    }
                    $return_array[] = array($val[0], $format);
                }
            }
        }
        $fp = fopen($path, 'w');
        foreach ($return_array as $campos) {
            fputcsv($fp, $campos);
        }
        fclose($fp);
        //$output['success'] = file_put_contents($csv);
        return file_get_contents($path); // Si ya lo tenemos lo parseamos!;
    }

}

?>