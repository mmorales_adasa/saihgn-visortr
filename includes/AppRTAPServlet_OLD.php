<?php

/**
 * Description of AppRTAPServlet
 *
 * @author francesc.ruiz
 */
class AppRTAPServlet {

    public $url;

    function __construct($url = "") {
        $this->url = $url;
    }

    function isXML($xml = "") {
        if (!empty($xml)) {
            libxml_use_internal_errors(TRUE);
            $doc = new DOMDocument('1.0', 'utf-8');
            $doc->loadXML($xml);
            $errors = libxml_get_errors();
            if (empty($errors)) {
                return TRUE;
            }
            $error = $errors[0];
            if ($error->level < 3) {
                return TRUE;
            }
            return FALSE;
        } else {
            return FALSE;
        }
    }

    public function get_xml($xml = "") {
        $estaciones = array();
        $order_row = array();
        $estaciones_extra = array();

        $datos_SAIH = simplexml_load_file($xml);

        foreach ($datos_SAIH->node as $node) {
            $tipo = (string) $node->attributes()->tipo;
            $label = (string) $node->attributes()->label;
            $descripcion = (string) $node->attributes()->descripcion;

            foreach ($node as $sub_node) {
                $grups_with_info = (string) $sub_node->attributes()->grupo;
                if ($grups_with_info == 'Estandar') {  // SOLO llamamos para los parámetros Estandar
                    foreach ($sub_node as $estacion) {
                        $str1 = (string) $estacion->attributes()->llamada; /// Llamadas para el webservice request 
                        $estaciones[$label][] = $str1;
						$str2 = (string) $estacion->attributes()->medida;
						// Niveles de Rio Absolutos
						if ($str2 == 'Nivel de Rio Absoluto') {
							$estaciones[$label][] = $str1;
                            $estaciones[$label][] = $label . '.Estado_com_gis';

                            //$estaciones_extra[$label]['Caudal']['caudales'][$parameter[0]]['descripcion_canal'] = $descripcion; //descripcion
                            $estaciones_extra[$label]['NRA']['tipo'] = 'NRA';  // Nivel de Rio Absoluto

                            $estaciones_extra[$label]['NRA']['label'] = $label;
                            $estaciones_extra[$label]['NRA']['descripcion'] = $descripcion;
                            $estaciones_extra[$label]['tipo'][] = 'NRA';  // Necesario para diferenciar los Niveles de Rio Absoluto
						}
                    }

                    $estaciones[$label][] = $label . '.Estado_com_gis';

                    $estaciones_extra[$label][$tipo]['descripcion'] = $descripcion;
                    $estaciones_extra[$label][$tipo]['tipo'] = $tipo;
                    $estaciones_extra[$label][$tipo]['label'] = $label;

                    $estaciones_extra[$label]['tipo'][] = $tipo;  // Necesario para diferenciar Embalse de canales
                } else if ($grups_with_info == 'NoEstandar') {  // SOLO llamamos para los parámetros NoEstandar
                    foreach ($sub_node as $estacion) {
                        $str1 = (string) $estacion->attributes()->llamada;

                        $str2 = (string) $estacion->attributes()->medida;
                        $check_call_descripcion = substr($str2, 0, 15);
                        $pieces_pre = explode('/', $str1);
                        $pieces = count($pieces_pre) > 1 ? $pieces_pre : explode('.', $str1);
                        $parameter = explode('.', $pieces[1]);

                        $caudales_salida = array('QWT.Caudal', 'QAT.Caudal', 'QFT.Caudal', 'QWR.Caudal', 'QWC.Caudal');
                        
                        // Caudales de Canal con descripción larga, son los que se muestran en la tabla de canales
                        if ($check_call_descripcion === 'Caudal de canal' AND strlen($str2) > 15) {

                            $estaciones[$label][] = $str1;
                            $estaciones[$label][] = $label . '.Estado_com_gis';

                            $desc_canal = substr($str2, 15, strlen($str2));
                            $estaciones_extra[$label]['Canal']['canales'][$parameter[0]]['descripcion_canal'] = $desc_canal; //descripcion
                            $estaciones_extra[$label]['Canal']['tipo'] = 'Canal';  // CANAL DE EMBALSE, dentro de Emblase (E)

                            $estaciones_extra[$label]['Canal']['label'] = $label;
                            $estaciones_extra[$label]['Canal']['descripcion'] = $descripcion;
                            $estaciones_extra[$label]['tipo'][] = 'Canal';  // Necesario para diferenciar los Caudales de Salida de los canales
						// Caudales de Salida
                        } else if (in_array($pieces[1], $caudales_salida)) {

                            $estaciones[$label][] = $str1;
                            $estaciones[$label][] = $label . '.Estado_com_gis';

                            // Comentado por MSR $desc_canal = substr($str2, 15, strlen($str2));
                            $estaciones_extra[$label]['Caudal']['caudales'][$parameter[0]]['descripcion_canal'] = $descripcion; //descripcion
                            $estaciones_extra[$label]['Caudal']['tipo'] = 'Caudal';  // caudal total, dentro de Emblase (E)

                            $estaciones_extra[$label]['Caudal']['label'] = $label;
                            $estaciones_extra[$label]['Caudal']['descripcion'] = $descripcion;
                            $estaciones_extra[$label]['tipo'][] = 'Caudal';  // Necesario para diferenciar los Caudales de Salida de los canales
                        }
                    }
                }
            }
        }

        $est_mount = $this->mount($estaciones);
        $is_xml = $this->isXML($est_mount);
        if ($is_xml) {
            $estacion = new SimpleXMLElement($est_mount);
        } else {
            $estacion = array();
        }

        $fields_canales = array('CA1.Caudal', 'QC1.Caudal', 'QC2.Caudal', 'QC3.Caudal');
        $caudales_salida = array('QWT.Caudal', 'QAT.Caudal', 'QFT.Caudal');
        foreach ($estacion as $value) {

            $str1 = (string) $value['varid'];
            $str2 = (string) $value['value'];
            $pieces_pre = explode('/', $str1);
            $pieces = count($pieces_pre) > 1 ? $pieces_pre : explode('.', $str1);
            $parameter = explode('.', $pieces[1]);

            foreach ($estaciones_extra[$pieces[0]]['tipo'] as $tipo) {

                if ($tipo === 'Canal' AND in_array($pieces[1], $fields_canales)) {
                    $order_row[$tipo][$pieces[0]]['canales'][$pieces[1]] = $str2;

                    $order_row[$tipo][$pieces[0]]['canales'][$parameter[0]]['descripcion_canal'] =
                            $estaciones_extra[$pieces[0]]['Canal']['canales'][$parameter[0]]['descripcion_canal'];
                } else if ($tipo === 'Caudal' AND in_array($pieces[1], $caudales_salida)) {
                    $order_row[$tipo][$pieces[0]][$pieces[1]] = $str2;
                } else {
                    $order_row[$tipo][$pieces[0]][$pieces[1]] = $str2;
                }


                $order_row[$tipo][$pieces[0]]['descripcion'] = $estaciones_extra[$pieces[0]][$tipo]['descripcion'];
                $order_row[$tipo][$pieces[0]]['label'] = $estaciones_extra[$pieces[0]][$tipo]['label'];

                $order_row[$tipo][$pieces[0]]['tipo'] = $tipo;
            }
        }

        return $order_row;
    }

    private function mount($estaciones = array()) {
        $xml = "";
        $output = "";
        if (is_array($estaciones)) {
            $xml = "<request>";
            foreach ($estaciones as $estacion) {
                foreach ($estacion as $llamada) {
// inicio petición
//recolección parámetros
                    $xml .= "<varid>";
                    $xml .= "$llamada";
                    $xml .= "</varid>";
// fin petición
                }
            }
            $xml .= "</request>";
//Inicio CURL peticion http 
            $ch = curl_init($this->url);
//      curl_setopt($ch, CURLOPT_MUTE, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));

            curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output .= curl_exec($ch);
            curl_close($ch);
        }

        return $output;
    }

    public function myCompare($obj1, $obj2) {
        if ($obj1['label'] == $obj2['label']) {
            return 0; // equal to
        } else if ($obj1['label'] < $obj2['label']) {
            return -1; // less than
        } else {
            return 1; // greater than
        }
    }

}