<?php

include ".././includes/graph.php";

$url = "http://172.21.0.4:2222/RTAPiWS/";
$station_type = 'E';

$station = isset($_GET['station']) ? rawurldecode($_GET['station']) : '';
$label = isset($_GET['label']) ? rawurldecode($_GET['label']) : ' ';

if (empty($station)) { // comprobamos que tiene los parámetros
    header('Location: ../index.php');  // sino redirigimos a la tabla
}

$graph = new graph($url, $station_type);


$data['variable_1'] = $graph->read_file($station, 'EMB.Nivel');
$data['variable_2'] = $graph->read_file($station, 'EMB.Volumen');

$values = $graph->parse_csv($data);
$csv = $values['data'];

$min_nivel = $values['variable_1']['min'] * 0.95;
$max_nivel = $values['variable_1']['max'] * 1.05;

$min_volumen = $values['variable_2']['min'] * 0.95;
$max_volumen = $values['variable_2']['max'] * 1.05;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="Shortcut Icon" href="../assets/img/favicon.ico"></link>
        <title>SAIH Guadiana - Gráficas históricas</title>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/charts_style.css" type="text/css">
        <script src="../assets/js/amcharts.js" type="text/javascript"></script>          
        <script type="text/javascript">       
            var chart;
            var graph;

<?php
$json = '';
$i = 0;
$length = count($csv);
foreach ($csv as $value) {

    if (!empty($value[0])) {
        $json .= '{year: new Date(' . (strtotime($value[0]) * 1000) . ')';
        $json .=!empty($value[1]) ? ',nivel:' . $value[1] : '';
        $json .=!empty($value[2]) ? ',volume:' . $value[2] : '';
        $json .= '}';
        if ($i < $length - 1) {
            $json .= ',';
        }
        $i++;
    }
}
?>
    var chartData = [<?php echo $json; ?>];	

    AmCharts.ready(function () {
        var chart = new AmCharts.AmSerialChart();
        chart.pathToImages = "images/";
        chart.dataProvider = chartData;
        chart.marginLeft = 10;
        chart.categoryField = "year";
        chart.zoomOutButton = 
            {
            backgroundColor: '#000000',
            backgroundAlpha: 0.15
        };
        chart.zoomOutText = "Ver todo";
        chart.panEventsEnabled = true;

        // AXES
        // category
        var categoryAxis = chart.categoryAxis;
        categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
        categoryAxis.minPeriod = "mm";
        categoryAxis.gridAlpha = 0.2;

        // PARAMETER 1
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.autoGridCount = true;
        valueAxis.includeGuidesInMinMax = true;
        valueAxis.title = "Nivel (m.s.n.m)";
        valueAxis.minimum = 0;
        valueAxis.axisAlpha = 1;
        valueAxis.axisColor = "#0000ff";
        valueAxis.gridAlpha = 0.2;
        valueAxis.gridColor = "#0000ff";
        valueAxis.inside = false;
        valueAxis.position="left";
        valueAxis.minimum=<?php echo $min_nivel ?>;
        valueAxis.maximum=<?php echo $max_nivel ?>;
        // valueAxis.digits_after_decimal=2;
        chart.addValueAxis(valueAxis);
               

        var graph = new AmCharts.AmGraph();
        graph.title="Nivel";                                
        graph.legendValueText="[[value]] (m.s.n.m)";
        graph.balloonText="N = [[value]] (m.s.n.m)";
        graph.hidden = false;					                 
//        graph.type = "smoothedLine"; // this line makes the graph smoothed line.
         
        graph.lineColor = "#0000ff"; // azul
        graph.lineThickness = 2;
        graph.valueField = "nivel";
            
        graph.valueAxis=valueAxis;
        chart.addGraph(graph);
                
        // GRAPH PARAMETER 2        
        var valueAxis2 = new AmCharts.ValueAxis();
        valueAxis2.autoGridCount = true;
        valueAxis2.includeGuidesInMinMax = true;                
        valueAxis2.title = "Volumen (hm3)";
        valueAxis2.offset = 1;					 
        valueAxis2.axisAlpha = 1;
        valueAxis2.axisColor = "#006633";
        valueAxis2.gridAlpha = 0.2;      
        valueAxis2.gridColor = "#006633";
        valueAxis2.inside = false;
        valueAxis2.position="right";      
        valueAxis2.minimum=<?php echo $min_volumen ?>;
        valueAxis2.maximum=<?php echo $max_volumen ?>;
        chart.addValueAxis(valueAxis2);                
                
        // VALUES    
        var graph2 = new AmCharts.AmGraph();
        graph2.title="Volumen ";                
        graph2.legendValueText="[[value]] (hm3)";
        graph2.balloonText="V = [[value]] (hm3)";
        graph2.hidden = false;
//        graph2.type = "smoothedLine"; // this line makes the graph smoothed line.
            
        graph2.lineColor = "#006633";  // verde

        graph2.lineThickness = 2;
        graph2.valueField = "volume";
        graph2.valueAxis = valueAxis2;            
        //    graph2.fillAlphas= 0.05;
        //   graph2.fillColor="#006633";    
        chart.addGraph(graph2);
                
                         
        // CURSOR
        var chartCursor = new AmCharts.ChartCursor();
        chartCursor.cursorAlpha = 0;
        chartCursor.cursorPosition = "mouse";
        chartCursor.categoryBalloonDateFormat = "HH:NN, DD-MM-YYYY (UTC)";
        chartCursor.color = "#000000";
        chartCursor.categoryBalloonColor = "#ccccff";
        chart.addChartCursor(chartCursor);

        // SCROLLBAR
        var chartScrollbar = new AmCharts.ChartScrollbar();
        chartScrollbar.graph = graph;
        chartScrollbar.autoGridCount = true;
        chartScrollbar.graphType = "smoothedLine";
        chartScrollbar.backgroundColor = "#EEEEEE";
        chartScrollbar.selectedBackgroundColor = "#CCCCFF";                
        chartScrollbar.gridAlpha = 0.3;
        chartScrollbar.gridColor = "#222222";
        chartScrollbar.color = "#000000";
        chartScrollbar.graphFillAlpha = 0.25;
        chartScrollbar.graphFillColor = "#383838";
        chartScrollbar.graphLineAlpha = "#383838";
        chartScrollbar.graphLineColor = "#383838";
        chartScrollbar.selectedGraphFillAlpha = 0.5;
        chartScrollbar.selectedGraphFillColor = "#66cccc";
        chartScrollbar.selectedGraphLineAlpha = "#66cccc";
        chartScrollbar.selectedGraphLineColor = "#66cccc";
        chartScrollbar.scrollbarHeight = 40;
        chart.addChartScrollbar(chartScrollbar);
                
        //LEGEND
        var legend = new AmCharts.AmLegend();
        legend.labelText = "[[title]]";
        legend.markerLabelGap = 32;
        legend.spacing = 16;
        legend.position="bottom";
        chart.addLegend(legend);
                
        //LOCALIZATION
        AmCharts.dayNames=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
        AmCharts.shortDayNames=['Dom','Lun','Mar','Mie','Jue','Vie','Sab'];
        AmCharts.monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        AmCharts.shortMonthNames=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
                
        chart.startEffect = "bounce";
        chart.sequencedAnimation = false;

        // WRITE
        chart.write("chartdiv");
    });
                      
        </script>
    </head>
    <body>
        <div class="list header">
            <h2>
                <center>                   
                    <?php echo $station ?> <?php echo $label; ?>
                </center>
            </h2>
            <h3>
                <center>           
                    Gráfica en tiempo real (7 últimos días)
                </center>
            </h3>
        </div>
    <center>
        <div id="chartdiv"></div>
    </center>

    <div>
        <?php
        if (isset($csv) AND !empty($csv)) {
            ?>
            <div class="tables_container">
                <div class="span5 pull-left">
                    <table class="datalist">
                        <thead>
                            <tr>
                                <th width="128">Fecha / Hora (UTC)</th>
                                <th width="80">Nivel (m)</th>                       
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $table_1 = $values['table_1'];
                            usort($table_1, 'my_sort_desc'); //ordeanmos ;
                            foreach ($table_1 as $value) {
                                ?>
                                <tr class="odd">
                                    <td class="date"><?php echo isset($value[0]) ? $value[0] : '' ?> (UTC) </td>
                                    <td class="value"><?php echo isset($value[1]) ? $value[1] : '' ?></td>                           
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
                <div class="span5 pull-right">
                    <table class="datalist">
                        <thead>
                            <tr>
                                <th width="128">Fecha / Hora (UTC)</th>                     
                                <th width="80">Volumen (hm3)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $table_2 = $values['table_2'];
                            usort($table_2, 'my_sort_desc'); //ordeanmos ;
                            foreach ($table_2 as $value) {
                                ?>
                                <tr class="odd">
                                    <td class="date"><?php echo isset($value[0]) ? $value[0] : '' ?> (UTC) </td>
                                    <td class="value"><?php echo isset($value[1]) ? $value[1] : '' ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        <?php }; ?>
    </div>
</body>
</html>