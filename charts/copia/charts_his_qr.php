<?php

include ".././includes/graph.php";
include ".././includes/db_pgsql.php";

$station = isset($_GET['station']) ? rawurldecode($_GET['station']) : '';
$label = isset($_GET['label']) ? rawurldecode($_GET['label']) : '';

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.
//$query_historic_values = "SELECT mediciones.med, mediciones.est, mediciones.data 
//FROM(
//SELECT tmd.data_medicion as med, tmd.cod_estacion as est,
//    round(CAST (tmd.valor_media AS numeric), 2) as data 
//    FROM tbl_olap_medidas_dias tmd 
//    WHERE tmd.cod_estacion = '" . $station . "' 
//    AND tmd.cod_variable = '" . $station . "/QR1' 
//    UNION
//    SELECT tmd.data_medicion as med, tmd.cod_estacion as est,
//    round(CAST (tmd.valor_media AS numeric), 2) as data
//    FROM tbl_olap_medidas_dias tmd 
//    WHERE  tmd.cod_estacion = '" . $station . "' 
//    AND tmd.cod_variable= '" . $station . "/NR1' 
//) AS mediciones 
//WHERE mediciones.data IS NOT NULL";

$query_historic_values = "SELECT tmd.data_medicion as med, tmd.cod_estacion as est,
    round(CAST (tmd.valor_media AS numeric), 2) as data 
    FROM tbl_olap_medidas_dias tmd 
    WHERE tmd.cod_estacion = '" . $station . "' 
    AND tmd.cod_variable = '" . $station . "/QR1' -- caudal
    ORDER BY tmd.data_medicion";

$query_historic_values_2 = "SELECT tmd.data_medicion as med, tmd.cod_estacion as est,
    round(CAST (tmd.valor_media AS numeric), 2) as data
    FROM tbl_olap_medidas_dias tmd 
    WHERE  tmd.cod_estacion = '" . $station . "' 
    AND tmd.cod_variable= '" . $station . "/NR1' -- nivel
    ORDER BY tmd.data_medicion";

$result_historic_values = $db_pgsql->query($query_historic_values);
$array_caudal = $db_pgsql->get_result_array($result_historic_values);

$result_historic_values_2 = $db_pgsql->query($query_historic_values_2);
$array_nivel = $db_pgsql->get_result_array($result_historic_values_2);

$count = count($array_caudal);
$mount = array();

for ($index = 0; $index < $count; $index++) {
    if (isset($array_caudal[$index]['data']) AND isset($array_nivel[$index]['data'])) {
        $mount[] = array('medicion' => $array_caudal[$index]['med'],
            'nivel' => $array_nivel[$index]['data'],
            'caudal' => $array_caudal[$index]['data']);
    }
}

$query_nivel = "   SELECT 
   MAX(  round(CAST (tmd.valor_media AS numeric), 2) ) as data_max,
   MIN(  round(CAST (tmd.valor_media AS numeric), 2) ) as data_min
    FROM tbl_olap_medidas_dias tmd
    WHERE  tmd.cod_estacion = '" . $station . "' 
    AND tmd.cod_variable= '" . $station . "/NR1' -- nivel ";

$result_nivel = $db_pgsql->query($query_nivel);
$data_array_nivel = $db_pgsql->get_result_array($result_nivel);

$query_caudal = "SELECT 
   MAX(  round(CAST (tmd.valor_media AS numeric), 2) ) as data_max,
   MIN(  round(CAST (tmd.valor_media AS numeric), 2) ) as data_min
    FROM tbl_olap_medidas_dias tmd
    WHERE tmd.cod_estacion ='" . $station . "' 
    AND tmd.cod_variable = '" . $station . "/QR1' -- caudal  ";

$result_caudal = $db_pgsql->query($query_caudal);
$data_array_caudal = $db_pgsql->get_result_array($result_caudal);



$min_nivel = $data_array_nivel[0]['data_min'] * 0.95;
$max_nivel = $data_array_nivel[0]['data_max'] * 1.15;

$min_caudal = $data_array_caudal[0]['data_min'] * 0.95; // $values['variable_2']['min'];
$max_caudal = $data_array_caudal[0]['data_max'] * 1.15;
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SAIH Guadiana - Gráficas históricas</title>
        <link rel="Shortcut Icon" href="../assets/img/favicon.ico"></link>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/charts_style.css" type="text/css">
        <script src="../assets/js/amcharts.js" type="text/javascript"></script>          
        <script type="text/javascript">       
            var chart;
            var graph;

            var chartData = [<?php
$i = 0;
$length = count($mount);
foreach ($mount as $value) {
    if (isset($value['medicion']) AND isset($value['nivel']) AND isset($value['caudal'])) {
        echo '{year: new Date(' . (strtotime($value['medicion']) * 1000) . '), caudal:'
        . $value['caudal'] . ', nivel:' . $value['nivel'] . '}';
        if ($i < $length - 1) {
            echo ',';
        }
        $i++;
    }
}
?>];

        AmCharts.ready(function () {
            var chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "images/";
            chart.dataProvider = chartData;
            chart.marginLeft = 10;
            chart.categoryField = "year";
            chart.zoomOutButton = 
                {
                backgroundColor: '#000000',
                backgroundAlpha: 0.15
            };
            chart.zoomOutText = "Ver todo";
            chart.panEventsEnabled = true;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "mm";
            categoryAxis.gridAlpha = 0.2;

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.autoGridCount = true;
            valueAxis.includeGuidesInMinMax = true;
            valueAxis.title = "Nivel (m)";
            valueAxis.minimum = 0;
            valueAxis.axisAlpha = 1;
            valueAxis.axisColor = "#0000ff";
            valueAxis.gridAlpha = 0.2;
            valueAxis.gridColor = "#0000ff";
            valueAxis.inside = false;
            valueAxis.position="left";
            valueAxis.minimum=<?php echo $min_nivel * 0.95; ?>;
            valueAxis.maximum=<?php echo $max_nivel * 1.05; ?>;
            chart.addValueAxis(valueAxis);
                
            var hiGuide = new AmCharts.Guide();
            hiGuide.lineColor = "#ffcc00";
            hiGuide.balloonColor = "#ffcc00";
            hiGuide.lineThickness = 2;                
            hiGuide.lineAlpha = 1;
            hiGuide.fillAlpha = 0.05;
            hiGuide.dashLength = 4;             
            hiGuide.fillColor = "#ffcc00";
            hiGuide.inside = true;
            hiGuide.balloonText = "Pre-aviso de nivel";
            hiGuide.label = "Pre-aviso N";
           
            // Aviso
            //hiGuide.toValue = hihilim1;
            //hiGuide.value = hilim1;
            
            valueAxis.addGuide(hiGuide);
                
            var hihiGuide = new AmCharts.Guide();
            hihiGuide.lineColor = "#ff0000";
            hihiGuide.balloonColor = "#ff0000";
            hihiGuide.lineThickness = 2;                
            hihiGuide.lineAlpha = 1;
            hihiGuide.fillAlpha = 0.05;
            hihiGuide.dashLength = 4;   					           
            hihiGuide.fillColor = "#ff0000";
            hihiGuide.inside = true;      
            hihiGuide.balloonText = "Alarma de nivel";
            hihiGuide.label = "Alarma N";
            
            // Aviso
            // hihiGuide.value = hihilim1;                
            valueAxis.addGuide(hihiGuide);
                
            var valueAxis2 = new AmCharts.ValueAxis();
            valueAxis2.autoGridCount = true;
            valueAxis2.includeGuidesInMinMax = true;                
            valueAxis2.title = "Caudal (m3/s)";
            valueAxis2.offset = 1;					 
            valueAxis2.axisAlpha = 1;
            valueAxis2.axisColor = "#006633";
            valueAxis2.gridAlpha = 0.2;      
            valueAxis2.gridColor = "#006633";
            valueAxis2.inside = false;
            valueAxis2.position="right";
            valueAxis2.minimum=<?php echo $min_caudal * 0.9; ?>;
            valueAxis2.maximum=<?php echo $max_caudal * 1.05; ?>;
            chart.addValueAxis(valueAxis2);
            //                
            // GRAPH                
            var graph = new AmCharts.AmGraph();
            graph.title="Nivel ";                                
            graph.legendValueText="[[value]] (m)";
            graph.balloonText="N = [[value]] (m)";
            graph.hidden = true;					                 
            graph.lineColor = "#0000ff";
        
            graph.lineThickness = 2;
            graph.valueField = "nivel";
            graph.valueAxis=valueAxis;

                
            var graph2 = new AmCharts.AmGraph();
            graph2.title="Caudal ";                
            graph2.legendValueText="[[value]] (m3/s)";
            graph2.balloonText="Q = [[value]] (m3/s)";
            graph2.hidden = false;
            graph2.fillAlphas= 0.05;
            graph2.fillColor="#006633";            
            graph2.lineColor = "#006633";
           
            graph2.lineThickness = 2;
            graph2.valueField = "caudal";
            graph2.valueAxis = valueAxis2;
            chart.addGraph(graph2);

            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.cursorPosition = "mouse";
            chartCursor.categoryBalloonDateFormat = "HH:NN, DD-MM-YYYY (UTC)";
            chartCursor.color = "#000000";
            chartCursor.categoryBalloonColor = "#ccccff";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chartScrollbar.graph = graph2;
            chartScrollbar.autoGridCount = true;
            chartScrollbar.graphType = "smoothedLine";
            chartScrollbar.backgroundColor = "#EEEEEE";
            chartScrollbar.selectedBackgroundColor = "#CCCCFF";                
            chartScrollbar.gridAlpha = 0.3;
            chartScrollbar.gridColor = "#222222";
            chartScrollbar.color = "#000000";
            chartScrollbar.graphFillAlpha = 0.25;
            chartScrollbar.graphFillColor = "#383838";
            chartScrollbar.graphLineAlpha = "#383838";
            chartScrollbar.graphLineColor = "#383838";
            chartScrollbar.selectedGraphFillAlpha = 0.5;
            chartScrollbar.selectedGraphFillColor = "#66cccc";
            chartScrollbar.selectedGraphLineAlpha = "#66cccc";
            chartScrollbar.selectedGraphLineColor = "#66cccc";
            chartScrollbar.scrollbarHeight = 40;
            chart.addChartScrollbar(chartScrollbar);
                
            //LEGEND
            var legend = new AmCharts.AmLegend();
            legend.labelText = "[[title]]";
            legend.markerLabelGap = 32;
            legend.spacing = 16;
            legend.position="bottom";
            chart.addLegend(legend);
                
            //LOCALIZATION
            AmCharts.dayNames=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
            AmCharts.shortDayNames=['Dom','Lun','Mar','Mie','Jue','Vie','Sab'];
            AmCharts.monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            AmCharts.shortMonthNames=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
                
            chart.startEffect = "bounce";
            chart.sequencedAnimation = false;

            // WRITE
            chart.write("chartdiv");
        });
                      
        </script>
    </head>
    <body>
        <div class="list header">
            <h2> <?php echo $station ?> <?php echo $label ?></h2>
            <h3>Gráfica de datos diarios (Año hidrológico actual).</h3>
        </div>
    <center><div id="chartdiv"></div></center>
    <p>
    <div>
        <center>
            <table class="datalist">
                <thead>
                    <tr>
                        <th width="128">Fecha</th>
                        <th width="80">Caudal (m3/s)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($mount) {
                        usort($mount, 'my_sort_desc'); //ordeanmos
                        foreach ($mount as $value) {
                            ?>
                            <tr class="odd">
                                <td class="date"><?php echo $value['medicion']; ?></td>                              
                                <td class="value"><?php echo $value['caudal']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>