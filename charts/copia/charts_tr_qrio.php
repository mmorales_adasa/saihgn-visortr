<?php

include ".././includes/graph.php";
include ".././includes/remote_db_pgsql.php";

$station = isset($_GET['station']) ? rawurldecode($_GET['station']) : '';
$label = isset($_GET['label']) ? rawurldecode($_GET['label']) : '';
// $parameter = isset($_GET['parameter']) ? rawurldecode($_GET['parameter']) : '';

//$array_translation = array('CA1' => 'NR1');

//$parameter = array_key_exists($parameter, $array_translation) === TRUE ? $array_translation[$parameter] : $parameter;

$parameter = 'QR1';

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

$query_historic_values = "SELECT timestamp_medicion as med, cod_estacion as est, cod_variable, 
round(CAST (valor AS numeric), 2) as data
FROM tbl_hist_medidas
WHERE cod_estacion ='" . $station . "' 
    AND cod_variable = '$station/$parameter' 
ORDER BY timestamp_medicion ASC";

$result_historic_values = $db_pgsql->query($query_historic_values);
$data_array = $db_pgsql->get_result_array($result_historic_values);

$mount = array();
for ($index = 0; $index < count($data_array); $index++) {
    if (isset($data_array[$index]['data'])) {
        $mount[] = array('medicion' => $data_array[$index]['med'],
            'caudal' => $data_array[$index]['data']);
    }
}

$query_caudal = "SELECT 
   MAX(  round(CAST (valor AS numeric), 2) ) as data_max,
   MIN(  round(CAST (valor AS numeric), 2) ) as data_min
   FROM tbl_hist_medidas
   WHERE cod_estacion ='" . $station . "' 
   AND cod_variable = '$station/$parameter' ";

$result_caudal = $db_pgsql->query($query_caudal);
$data_array_caudal = $db_pgsql->get_result_array($result_caudal);


$min_caudal = $data_array_caudal[0]['data_min']; // $values['variable_2']['min'];
$max_caudal = $data_array_caudal[0]['data_max'];
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="Shortcut Icon" href="../assets/img/favicon.ico"></link>
        <title>SAIH Guadiana - Gráfica TR</title>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/charts_style.css" type="text/css">
        <script src="../assets/js/amcharts.js" type="text/javascript"></script>          
        <script type="text/javascript">       
            var chart;
            var graph;

            var chartData = [<?php
$i = 0;
$length = count($mount);
foreach ($mount as $value) {
    if (isset($value['medicion']) AND isset($value['caudal'])) {
        echo '{year: new Date(' . (strtotime($value['medicion']) * 1000) . '), caudal:'
        . $value['caudal'] . '}';
        if ($i < $length - 1) {
            echo ',';
        }
        $i++;
    }
}
?>];

        AmCharts.ready(function () {
            var chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "images/";
            chart.dataProvider = chartData;
            chart.marginLeft = 10;
            chart.categoryField = "year";
            chart.zoomOutButton = 
                {
                backgroundColor: '#000000',
                backgroundAlpha: 0.15
            };
            chart.zoomOutText = "Ver todo";
            chart.panEventsEnabled = true;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "mm";
            categoryAxis.gridAlpha = 0.2;

            // PARAMETER 1
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.autoGridCount = true;
            valueAxis.includeGuidesInMinMax = true;
            valueAxis.title = "Caudal (m)";
            valueAxis.minimum = 0;
            valueAxis.axisAlpha = 1;
            valueAxis.axisColor = "#0000ff";
            valueAxis.gridAlpha = 0.2;
            valueAxis.gridColor = "#0000ff";
            valueAxis.inside = false;
            valueAxis.position="left";
            valueAxis.minimum=<?php echo $min_caudal ?>;
            valueAxis.maximum=<?php echo $max_caudal ?>;
            // valueAxis.digits_after_decimal=2;
            chart.addValueAxis(valueAxis);
               

            var graph = new AmCharts.AmGraph();
            graph.title="Caudal";                                
            graph.legendValueText="[[value]] (m)";
            graph.balloonText="Q = [[value]] m)";
            graph.hidden = false;					                 
//            graph.type = "smoothedLine"; // this line makes the graph smoothed line.
         
            graph.lineColor = "#0000ff"; // azul
       
            graph.lineThickness = 2;
            graph.valueField = "caudal";
            
            graph.valueAxis=valueAxis;
            chart.addGraph(graph);       
       
                         
            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.cursorPosition = "mouse";
            chartCursor.categoryBalloonDateFormat = "HH:NN, DD-MM-YYYY (UTC)";
            chartCursor.color = "#000000";
            chartCursor.categoryBalloonColor = "#ccccff";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chartScrollbar.graph = graph;
            chartScrollbar.autoGridCount = true;
            chartScrollbar.graphType = "smoothedLine";
            chartScrollbar.backgroundColor = "#EEEEEE";
            chartScrollbar.selectedBackgroundColor = "#CCCCFF";                
            chartScrollbar.gridAlpha = 0.3;
            chartScrollbar.gridColor = "#222222";
            chartScrollbar.color = "#000000";
            chartScrollbar.graphFillAlpha = 0.25;
            chartScrollbar.graphFillColor = "#383838";
            chartScrollbar.graphLineAlpha = "#383838";
            chartScrollbar.graphLineColor = "#383838";
            chartScrollbar.selectedGraphFillAlpha = 0.5;
            chartScrollbar.selectedGraphFillColor = "#66cccc";
            chartScrollbar.selectedGraphLineAlpha = "#66cccc";
            chartScrollbar.selectedGraphLineColor = "#66cccc";
            chartScrollbar.scrollbarHeight = 40;
            chart.addChartScrollbar(chartScrollbar);
                
            //LEGEND
            var legend = new AmCharts.AmLegend();
            legend.labelText = "[[title]]";
            legend.markerLabelGap = 32;
            legend.spacing = 16;
            legend.position="bottom";
            chart.addLegend(legend);
                
            //LOCALIZATION
            AmCharts.dayNames=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
            AmCharts.shortDayNames=['Dom','Lun','Mar','Mie','Jue','Vie','Sab'];
            AmCharts.monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            AmCharts.shortMonthNames=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
                
            chart.startEffect = "bounce";
            chart.sequencedAnimation = false;

            // WRITE
            chart.write("chartdiv");
        });
                      
        </script>
    </head>
    <body>
        <div class="list header">
            <h2> <?php echo $station ?> <?php echo $label; ?></h2>
            <h3>Gráfica en tiempo real (7 últimos días)</h3>
        </div>
    <center>
        <div id="chartdiv"></div>
    </center>
    <p>
    <div>
        <center>
            <table class="datalist">
                <thead>
                    <tr>
                        <th width="120">Fecha</th>
                        <th width="120">Caudal Río(m3/s)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($mount) AND !empty($mount)) {
                        usort($mount, 'my_sort_desc'); //ordeanmos
                        foreach ($mount as $value) {
                            ?>
                            <tr class="odd">
                                <td class="date"><?php echo $value['medicion'] ?></td>
                                <td class="value"><?php echo $value['caudal'] ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>