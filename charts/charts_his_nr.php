<?php

include ".././includes/graph.php";
include ".././includes/db_pgsql.php";

$station = isset($_GET['station']) ? rawurldecode($_GET['station']) : '';
$label = isset($_GET['label']) ? rawurldecode($_GET['label']) : '';
$parameter = isset($_GET['parameter']) ? rawurldecode($_GET['parameter']) : '';

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

$query_historic_values = "SELECT data_medicion as med,
    round(CAST (valor_media AS numeric), 2) as data 
    FROM tbl_olap_medidas_dias
    WHERE cod_variable = '" . $station . "/NR1' --nivel
    ORDER BY data_medicion";	

$result_historic_values = $db_pgsql->query($query_historic_values);
$array_historic = $db_pgsql->get_result_array($result_historic_values);

$mount = array();
$min_nivel = 9999;
$max_nivel = 0;
for ($index = 0; $index < count($array_historic); $index++) {
	if (isset($array_historic[$index]['data'])) {
		$min_nivel = $array_historic[$index]['data'] < $min_nivel ? $array_historic[$index]['data']:$min_nivel;
		$max_nivel = $array_historic[$index]['data'] > $max_nivel ? $array_historic[$index]['data']:$max_nivel;
		$mount[] = array('medicion' =>$array_historic[$index]['med'],'nivel' => $array_historic[$index]['data']);
	}
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="Shortcut Icon" href="../assets/img/favicon.ico"></link>
        <title>SAIH Guadiana - Gráficas históricas</title>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/charts_style.css" type="text/css">
        <script src="../assets/js/amcharts.js" type="text/javascript"></script>          
        <script type="text/javascript">       
            var chart;
            var graph;

            var chartData = [<?php
$i = 0;
$length = count($mount);
foreach ($mount as $value) {
        echo '{year: new Date(' . (strtotime($value['medicion']) * 1000) . '), nivel:'
        . $value['nivel'] . '}';
        if ($i < $length - 1) echo ',';
        $i++;
}
?>];

        AmCharts.ready(function () {
            var chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "images/";
            chart.dataProvider = chartData;
            chart.marginLeft = 10;
            chart.categoryField = "year";
            chart.zoomOutButton = 
                {
                backgroundColor: '#000000',
                backgroundAlpha: 0.15
            };
            chart.zoomOutText = "Ver todo";
            chart.panEventsEnabled = true;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "mm";
            categoryAxis.gridAlpha = 0.2;

            // PARAMETER 1
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.autoGridCount = true;
            valueAxis.includeGuidesInMinMax = true;
            valueAxis.title = "Nivel Río (m)";
            valueAxis.minimum = 0;
            valueAxis.axisAlpha = 1;
            valueAxis.axisColor = "#0000ff";
            valueAxis.gridAlpha = 0.2;
            valueAxis.gridColor = "#0000ff";
            valueAxis.inside = false;
            valueAxis.position="left";
            valueAxis.minimum=<?php echo $min_nivel ?>;
            valueAxis.maximum=<?php echo $max_nivel ?>;
            // valueAxis.digits_after_decimal=2;
            chart.addValueAxis(valueAxis);
               

            var graph = new AmCharts.AmGraph();
            graph.title="Nivel";                                
            graph.legendValueText="[[value]] (m)";
            graph.balloonText="N = [[value]] m)";
            graph.hidden = false;					                 
//            graph.type = "smoothedLine"; // this line makes the graph smoothed line.
         
            graph.lineColor = "#0000ff"; // azul
       
            graph.lineThickness = 2;
            graph.valueField = "nivel";
            
            graph.valueAxis=valueAxis;
            chart.addGraph(graph);       
       
                         
            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.cursorPosition = "mouse";
            chartCursor.categoryBalloonDateFormat = "JJ:NN, DD-MM-YYYY (UTC)";
            chartCursor.color = "#000000";
            chartCursor.categoryBalloonColor = "#ccccff";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chartScrollbar.graph = graph;
            chartScrollbar.autoGridCount = true;
            chartScrollbar.graphType = "smoothedLine";
            chartScrollbar.backgroundColor = "#EEEEEE";
            chartScrollbar.selectedBackgroundColor = "#CCCCFF";                
            chartScrollbar.gridAlpha = 0.3;
            chartScrollbar.gridColor = "#222222";
            chartScrollbar.color = "#000000";
            chartScrollbar.graphFillAlpha = 0.25;
            chartScrollbar.graphFillColor = "#383838";
            chartScrollbar.graphLineAlpha = "#383838";
            chartScrollbar.graphLineColor = "#383838";
            chartScrollbar.selectedGraphFillAlpha = 0.5;
            chartScrollbar.selectedGraphFillColor = "#66cccc";
            chartScrollbar.selectedGraphLineAlpha = "#66cccc";
            chartScrollbar.selectedGraphLineColor = "#66cccc";
            chartScrollbar.scrollbarHeight = 40;
            chart.addChartScrollbar(chartScrollbar);
                
            //LEGEND
            var legend = new AmCharts.AmLegend();
            legend.labelText = "[[title]]";
            legend.markerLabelGap = 32;
            legend.spacing = 16;
            legend.position="bottom";
            chart.addLegend(legend);
                
            //LOCALIZATION
            AmCharts.dayNames=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
            AmCharts.shortDayNames=['Dom','Lun','Mar','Mie','Jue','Vie','Sab'];
            AmCharts.monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            AmCharts.shortMonthNames=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
                
            chart.startEffect = "bounce";
            chart.sequencedAnimation = false;

            // WRITE
            chart.write("chartdiv");
        });
                      
        </script>
    </head>
    <body>
        <div class="list header">
            <h2> <?php echo $station ?> <?php echo htmlentities($label); ?></h2>
            <h3>Gráfica de datos diarios (Año hidrológico actual).</h3>
        </div>
    <center>
        <div id="chartdiv"></div>
    </center>
    <p>
    <div>
        <center>
            <table class="datalist">
                <thead>
                    <tr>
                        <th width="128">Fecha</th>
                        <th width="80">Nivel (m)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (isset($mount) AND !empty($mount)) {
                        usort($mount, 'my_sort_desc'); //ordeanmos
                        foreach ($mount as $value) {
                            ?>
                            <tr class="odd">
                                <td class="date"><?php echo $value['medicion'] ?></td>
                                <td class="value"><?php echo $value['nivel'] ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>