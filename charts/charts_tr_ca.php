<?php

include ".././includes/graph.php";
include ".././includes/db_pgsql.php";

$station = isset($_GET['station']) ? rawurldecode($_GET['station']) : '';
$label = isset($_GET['label']) ? rawurldecode($_GET['label']) : '';
$parameter = isset($_GET['parameter']) ? rawurldecode($_GET['parameter']) : '';

if ($parameter == 'CA1') $parameter = 'QC1';
$parameter = '/' . $parameter;

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

$query_historic_values = "SELECT instante_med as med,
	round(CAST (valor AS numeric), 2) as data 
	FROM tbl_dat_minutales
	WHERE cod_variable = '" . $station . $parameter . "' AND instante_med > now()-'7 day'::interval
	ORDER BY instante_med";	

$result_historic_values = $db_pgsql->query($query_historic_values);
$array_historic = $db_pgsql->get_result_array($result_historic_values);

$mount = array();
$min_caudal = 9999;
$max_caudal = 0;
for ($index = 0; $index < count($array_historic); $index++) {
	if (isset($array_historic[$index]['data'])) {
		$min_caudal = $array_historic[$index]['data'] < $min_caudal ? $array_historic[$index]['data']:$min_caudal;
		$max_caudal = $array_historic[$index]['data'] > $max_caudal ? $array_historic[$index]['data']:$max_caudal;
		$mount[] = array('medicion' =>$array_historic[$index]['med'],'caudal' => $array_historic[$index]['data']);
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>SAIH Guadiana - Gráficas históricas</title>
        <link rel="Shortcut Icon" href="../assets/img/favicon.ico"></link>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="../assets/css/charts_style.css" type="text/css">
        <script src="../assets/js/amcharts.js" type="text/javascript"></script>          
        <script type="text/javascript">       
            var chart;
            var graph;

            var chartData = [<?php
$i = 0;
$length = count($mount);
foreach ($mount as $value) {
        echo '{year: new Date(' . (strtotime($value['medicion']) * 1000) . '), caudal:'
        . $value['caudal'] . '}';
        if ($i < $length - 1) echo ',';
        $i++;
}
?>];

        AmCharts.ready(function () {
            var chart = new AmCharts.AmSerialChart();
            chart.pathToImages = "images/";
            chart.dataProvider = chartData;
            chart.marginLeft = 10;
            chart.categoryField = "year";
            chart.zoomOutButton = 
                {
                backgroundColor: '#000000',
                backgroundAlpha: 0.15
            };
            chart.zoomOutText = "Ver todo";
            chart.panEventsEnabled = true;

            // AXES
            // category
            var categoryAxis = chart.categoryAxis;
            categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
            categoryAxis.minPeriod = "mm";
            categoryAxis.gridAlpha = 0.2;

            // value
            var valueAxis = new AmCharts.ValueAxis();
            valueAxis.autoGridCount = true;
            valueAxis.includeGuidesInMinMax = true;
            valueAxis.title = "Caudal (m³/s)";
            valueAxis.minimum = 0;
            valueAxis.axisAlpha = 1;
            valueAxis.axisColor = "#0000ff";
            valueAxis.gridAlpha = 0.2;
            valueAxis.gridColor = "#0000ff";
            valueAxis.inside = false;
            valueAxis.position="left";
            valueAxis.minimum=<?php echo $min_caudal * 0.95; ?>;
            valueAxis.maximum=<?php echo $max_caudal * 1.05; ?>;
            chart.addValueAxis(valueAxis);
                
            var hiGuide = new AmCharts.Guide();
            hiGuide.lineColor = "#ffcc00";
            hiGuide.balloonColor = "#ffcc00";
            hiGuide.lineThickness = 2;                
            hiGuide.lineAlpha = 1;
            hiGuide.fillAlpha = 0.05;
            hiGuide.dashLength = 4;             
            hiGuide.fillColor = "#ffcc00";
            hiGuide.inside = true;
            hiGuide.balloonText = "Pre-aviso de caudal";
            hiGuide.label = "Pre-aviso Q";
           
            // Aviso
            //hiGuide.toValue = hihilim1;
            //hiGuide.value = hilim1;
            
            valueAxis.addGuide(hiGuide);
                
            var hihiGuide = new AmCharts.Guide();
            hihiGuide.lineColor = "#ff0000";
            hihiGuide.balloonColor = "#ff0000";
            hihiGuide.lineThickness = 2;                
            hihiGuide.lineAlpha = 1;
            hihiGuide.fillAlpha = 0.05;
            hihiGuide.dashLength = 4;   					           
            hihiGuide.fillColor = "#ff0000";
            hihiGuide.inside = true;      
            hihiGuide.balloonText = "Alarma de caudal";
            hihiGuide.label = "Alarma Q";
            
            // Aviso
            // hihiGuide.value = hihilim1;                
            valueAxis.addGuide(hihiGuide);
                
            //                
            // GRAPH                
            var graph = new AmCharts.AmGraph();
            graph.title="Caudal";                                
            graph.legendValueText="[[value]] (m³/s)";
            graph.balloonText="Q = [[value]] (m³/s)";
            graph.hidden = false;					                 
            graph.lineColor = "#0000ff";
        
            graph.lineThickness = 2;
            graph.valueField = "caudal";
            graph.valueAxis=valueAxis;
            chart.addGraph(graph);
                
            // CURSOR
            var chartCursor = new AmCharts.ChartCursor();
            chartCursor.cursorAlpha = 0;
            chartCursor.cursorPosition = "mouse";
            chartCursor.categoryBalloonDateFormat = "JJ:NN, DD-MM-YYYY (UTC)";
            chartCursor.color = "#000000";
            chartCursor.categoryBalloonColor = "#ccccff";
            chart.addChartCursor(chartCursor);

            // SCROLLBAR
            var chartScrollbar = new AmCharts.ChartScrollbar();
            chartScrollbar.graph = graph;
            chartScrollbar.autoGridCount = true;
            chartScrollbar.graphType = "smoothedLine";
            chartScrollbar.backgroundColor = "#EEEEEE";
            chartScrollbar.selectedBackgroundColor = "#CCCCFF";                
            chartScrollbar.gridAlpha = 0.3;
            chartScrollbar.gridColor = "#222222";
            chartScrollbar.color = "#000000";
            chartScrollbar.graphFillAlpha = 0.25;
            chartScrollbar.graphFillColor = "#383838";
            chartScrollbar.graphLineAlpha = "#383838";
            chartScrollbar.graphLineColor = "#383838";
            chartScrollbar.selectedGraphFillAlpha = 0.5;
            chartScrollbar.selectedGraphFillColor = "#66cccc";
            chartScrollbar.selectedGraphLineAlpha = "#66cccc";
            chartScrollbar.selectedGraphLineColor = "#66cccc";
            chartScrollbar.scrollbarHeight = 40;
            chart.addChartScrollbar(chartScrollbar);
                
            //LEGEND
            var legend = new AmCharts.AmLegend();
            legend.labelText = "[[title]]";
            legend.markerLabelGap = 32;
            legend.spacing = 16;
            legend.position="bottom";
            chart.addLegend(legend);
                
            //LOCALIZATION
            AmCharts.dayNames=['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'];
            AmCharts.shortDayNames=['Dom','Lun','Mar','Mie','Jue','Vie','Sab'];
            AmCharts.monthNames=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            AmCharts.shortMonthNames=['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
                
            chart.startEffect = "bounce";
            chart.sequencedAnimation = false;

            // WRITE
            chart.write("chartdiv");
        });
                      
        </script>
    </head>
    <body>
        <div class="list header">
            <h2> <?php echo $station ?> <?php echo htmlentities($label) ?></h2>
            <h3>Gráfica en tiempo real (7 últimos días)</h3>
        </div>
    <center><div id="chartdiv"></div></center>
    <p>
    <div>
        <center>
            <table class="datalist">
                <thead>
                    <tr>
                        <th width="128">Fecha</th>
                        <th width="80">Caudal (m³/s)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if ($mount) {
                        usort($mount, 'my_sort_desc'); //ordenamos desc para tabla datos
                        foreach ($mount as $value) {
                            ?>
                            <tr class="odd">
                                <td class="date"><?php echo $value['medicion']; ?></td>                              
                                <td class="value"><?php echo $value['caudal']; ?></td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </center>
    </div>
</body>
</html>