<?php

error_reporting(E_ALL);
require_once "../includes/util.php";
require_once "../includes/AppRTAPServlet.php";
require_once "../includes/db_pgsql.php";
require_once '../includes/class.email.php'; //

$request = new AppRTAPServlet("http://172.21.0.4:2222/RTAPiWS/GetVariablesValues");
$estaciones = $request->get_xml('../xmls/alarmas.xml'); //../xmls/datos_estaciones.xml

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

$query_alarmas = "SELECT * FROM tbl_alarmas ala, tbl_alarmas_variables var
                  WHERE  var.variable = ala.cod_unidad AND habilitada 
                  ORDER BY cod_variable desc";
$result_query_alarmas = $db_pgsql->query($query_alarmas);
$alarmas_array = $db_pgsql->get_result_array($result_query_alarmas);

if (!empty($estaciones) AND !empty($alarmas_array)) {
    foreach ($alarmas_array as $alarma) {// Por cada alarma en la BBDD miramos los umbrales respecto a las estaciones
        $cod_variable = isset($alarma['cod_variable']) ? $alarma['cod_variable'] : '';
        $cod_variable_array = explode('/', $cod_variable);
        $cod_estacion = isset($cod_variable_array[0]) ? $cod_variable_array[0] : '';
        $cod_variable_1 = isset($cod_variable_array[1]) ? $cod_variable_array[1] : '';


        $tipo = isset($alarma['tipo']) ? $alarma['tipo'] : '';
        $indice_alarma = isset($alarma['indice_alarma']) ? $alarma['indice_alarma'] : '';

        $tipo_estacion = isset($alarma['tipo_estacion']) ? $alarma['tipo_estacion'] : '';
        $umbral = isset($alarma['umbral']) ? $alarma['umbral'] : '';

        $enviada = isset($alarma['enviada']) ? $alarma['enviada'] : '0';
        $umbral_activado = isset($alarma['umbral_activado']) ? $alarma['umbral_activado'] : '';
        $usuario = isset($alarma['usuario']) ? $alarma['usuario'] : '';
        $correo_para = isset($alarma['correo_para']) ? $alarma['correo_para'] : '';
        $correo_cc = isset($alarma['correo_cc']) ? $alarma['correo_cc'] : '';
        $unidad = isset($alarma['unidad']) ? $alarma['unidad'] : '';

        $current_value = isset($estaciones[$tipo_estacion][$cod_estacion][$cod_variable_1]) ? $estaciones[$tipo_estacion][$cod_estacion][$cod_variable_1] : ''; // valor actual de la estación dato por el Rtap
        if (!empty($current_value)) {
            $current_value = round_number($current_value);
            $umbral = round_number($umbral);
            if ($enviada === '0') { // Comprobamos so hay alertas
                if ($current_value > $umbral AND $tipo === 'superior') {// valor actual mayor  que el máximo
                    $send_alert = "Alarma creada por $usuario ($correo_para) <br/>
                       Activación alarma para la variable '$cod_variable' con valor " . round_number($current_value) . " $unidad" .
                            "<br />  La alarma cesará cuando baje de " . round_number($umbral) . " $unidad.";

                    $subject = "Alarmas SAIHGuadiana: Activada - $cod_variable = " . round_number($current_value) . " $unidad";
                    $success = send_mail($subject, $send_alert, $correo_para, $correo_cc);
                    //
                    if ($success === TRUE) {
                        activate_alarm($tipo_estacion, $cod_variable, $indice_alarma, 'superior', $usuario);
                    }
                } else if ($current_value < $umbral AND $tipo === 'inferior') {// valor actual menor  que el mínimo
                    $send_alert = "Alarma creada por $usuario ($correo_para) <br/>
                        Activación alarma para la variable '$cod_variable' con valor " . round_number($current_value) . " $unidad" .
                            "<br />  La alarma cesará cuando suba de " . round_number($umbral) . " $unidad.";

                    $subject = "SAIHGuadiana: Activada - $cod_variable = " . round_number($current_value) . " $unidad";
                    $success = send_mail($subject, $send_alert, $correo_para, $correo_cc);
                    if ($success === TRUE) {
                        activate_alarm($tipo_estacion, $cod_variable, $indice_alarma, 'inferior', $usuario);
                    }
                }
            } else if ($enviada === '1') { // comprobamos que los valores están cambiados ahora
                // valor actual menor que el máximo
                if ($current_value < $umbral AND $tipo === 'superior') {
                    $send_alert = "Alarma creada por $usuario ($correo_para) <br/>
                        Desactivación alarma para la variable '$cod_variable' con valor " . round_number($current_value) . " $unidad" .
                            "<br />  La alarma ha cesado al bajar del máximo establecido " . round_number($umbral) . " $unidad.";

                    $subject = "SAIHGuadiana: Desactivada - $cod_variable = " . round_number($current_value) . " $unidad";
                    $success = send_mail($subject, $send_alert, $correo_para, $correo_cc);
                    if ($success === TRUE) {
                        deactivate_alarm($tipo_estacion, $cod_variable, $indice_alarma, 'desactivada', $usuario);
                    }
                    // comprobamos que los valores están cambiados ahora
                } else if ($current_value > $umbral AND $tipo === 'inferior') {// valor actual mayor  que el mínimo
                    $send_alert = "Alarma creada por $usuario ($correo_para) <br/>
                        Desactivación alarma para la variable '$cod_variable' con valor " . round_number($current_value) . " $unidad" .
                            "<br /> La alarma ha cesado al subir del mínimo establecido " . round_number($umbral) . " $unidad.";

                    $subject = "SAIHGuadiana: Desactivada - $cod_variable = " . round_number($current_value) . " $unidad";
                    $success = send_mail($subject, $send_alert, $correo_para, $correo_cc);
                    if ($success === TRUE) {
                        deactivate_alarm($tipo_estacion, $cod_variable, $indice_alarma, 'desactivada', $usuario);
                    }
                }
            }
        }
    }
}

// Función crea el Mail (mails/tempaltes) y lo envía
function send_mail($subject = '', $alert_message = '', $to = '', $cc = '') {
    $mail_obj = new Email();
    $mail_template = './correos/plantilla.php';

// buscamos el mensaje
    if (file_exists($mail_template)) {
        $template = file_get_contents($mail_template);
        $cod_variables = array("%template%");
        $change = array($alert_message);
        $send_message = str_replace($cod_variables, $change, $template);
    } else {
        $send_message = "The file $mail_template does not exist";
    }

    $to = str_replace(';', ',', $to);
    //$to = str_replace('/\s+/', ',', $to);

    $cc = str_replace(';', ',', $cc);
    // $cc = str_replace('/\s+/', ',', $cc);

    $success = @$mail_obj->send($to, $subject, $send_message, $cc); // putting @ prevent "Non-static method Mail::factory() should not be called statically"
    date_default_timezone_set('Europe/Madrid');
    if ($success !== TRUE) {

        $fecha = date("Y-m-d h:i:s");
        error_log("$fecha El envío de correo falló  revisalo =>  \n  \n"
                , 3, "./logs/error_mails.log");
        return FALSE;
    } else {
        $fecha = date("Y-m-d h:i:s");
        error_log("$fecha Enviado correo a $to con descripción =>  \n  $alert_message \n \n"
                , 3, "./logs/mails.log");
        return TRUE;
    }
}

/*
  Estado
  --    0 significa que la alerta está Desactivada.
  --    1 significa que la alerta está Activada.
 */

function activate_alarm($station = '', $cod_variable = '', $indice_alarma = '', $umbral_activado = '', $usuario = '') {
    $db_pgsql = db_pgsql::singleton();
    $db_pgsql->connect(); // conexión al schema en config.ini => db_schema.
    $query = "UPDATE tbl_alarmas SET enviada = '1', umbral_activado = '$umbral_activado' 
                              WHERE indice_alarma = '$indice_alarma' AND  tipo_estacion='$station' AND usuario = '$usuario' 
                                  AND cod_variable ='$cod_variable' ;";
    $success = $db_pgsql->query($query);
    return $success;
}

function deactivate_alarm($station = '', $cod_variable = '', $indice_alarma = '', $umbral_activado = '', $usuario = '') {
    $db_pgsql = db_pgsql::singleton();
    $db_pgsql->connect(); // conexión al schema en config.ini => db_schema.
    $query = "UPDATE tbl_alarmas SET enviada = '0', umbral_activado = '$umbral_activado' 
                              WHERE indice_alarma = '$indice_alarma'AND tipo_estacion='$station' AND usuario = '$usuario'
                                  AND cod_variable ='$cod_variable' ;";
    $success = $db_pgsql->query($query);
    return $success;
}

?>