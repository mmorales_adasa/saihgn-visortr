<?php
include "../login/login.php";

require_once "../includes/util.php";
require_once "../includes/db_pgsql.php";

$cod_estacion = isset($_GET['cod_estacion']) ? rawurldecode($_GET['cod_estacion']) : '';
$tipo_estacion = isset($_GET['tipo_estacion']) ? rawurldecode($_GET['tipo_estacion']) : '';
$descripcion = isset($_GET['descripcion']) ? rawurldecode($_GET['descripcion']) : 'Estación';
$usuario = isset($_SESSION['usuario']) ? $_SESSION['usuario'] : '';

if (empty($cod_estacion) OR empty($tipo_estacion) OR empty($usuario)) { // comprobamos que tiene los parámetros
    header('Location: ../index.php');  // sino redirigimos a la tabla
} else {

    $db_pgsql = new db_pgsql;
    $db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

    $query_alarms = "SELECT * FROM tbl_alarmas
                 WHERE cod_estacion='$cod_estacion' AND usuario = '$usuario'
                            ORDER BY  cod_variable desc";
    $result_query_alarms = $db_pgsql->query($query_alarms);
    $alarms_array = $db_pgsql->get_result_array($result_query_alarms);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
        <title>SAIH Guadiana - Consulta de alarmas</title>
        <link rel="Shortcut Icon" href="../assets/img/favicon.ico"></link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="stylesheet" href="../assets/css/jquery/jquery-ui-1.9.2.custom.min.css" type="text/css"></link>
        <link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css"></link>
        <link rel="stylesheet" href="../assets/css/style.css" type="text/css"></link>
    </head>
    <body>
        <div class="list header">
            <h2>
                <center><?php echo $descripcion; ?></center>
            </h2>
            <h3>
                <center>           
                    Gestión de alertas
                </center>
            </h3>
        </div>
        <!--FIN CABECERA-->
        <!--TABLAS-->

        <div id="container_alarmas" >
            <div   style="height: 60px;"  >
                <div  class="div_informacio alert hide "></div>
            </div> 
            <div class="list"> 
                <div class="container"> <!--  -->
                    <div class="row" style="margin: 5px 0 25px 0"> <!--  -->
                        <input type="button" 
                               data-url="../alarmas/html/formulario/nueva_alarma.php"
                               data-estacion="<?php echo $cod_estacion; ?>"
                               data-tipo="<?php echo $tipo_estacion; ?>"
                               data-usuario="<?php echo $usuario; ?>"
                               class="nueva_alarma btn btn-primary" value="Nueva alarma" name="nueva_alarma" />
                    </div>
                </div>

                <div class="tbody"> <!--  -->
                    <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
                        <thead>
                            <th class="sub" width="10%">Variable</th>
                            <th class="sub" width="10%">Tipo</th>
                            <th class="sub" width="10%">Umbral</th>
                            <th class="sub" width="10%">habilitada</th>
                            <th class="sub" width="10%">Enviada</th>
                            <th class="sub" width="15%">Para</th>
                            <th class="sub" width="15%">CC</th>
                            <th class="sub" width="10%">Editar</th>
                            <th class="sub" width="10%">Eliminar</th>
                        </thead> 
                        <tbody>
                            <?php
                            foreach ($alarms_array as $alarm) {
                                ?>
                                <tr style="height: 30px">  

                                    <td class="align_center" width="10%">
                                        <?php echo $alarm['cod_variable'] ?>
                                    </td>                     

                                    <td class="align_center" width="10%">
                                        <?php echo $alarm['tipo'] ?>
                                    </td>  

                                    <td class="align_center" width="10%">                
                                        <?php echo round_number($alarm['umbral']); ?>
                                    </td>  

                                    <td class="align_center" width="10%">
                                        <?php
                                        $alarma_activada = $alarm['habilitada'] === 't' ? 'habilitada' : 'deshabiltida';
                                        echo $alarma_activada;
                                        ?>
                                    </td>

                                    <td class="align_center" width="10%">
                                        <?php
                                        switch ($alarm['enviada']) {
                                            case 0:
                                                echo 'No enviada';
                                                break;
                                            case 1:
                                                echo 'Enviada';
                                                break;
                                            case 2:
                                                echo 'Enviada';
                                                break;
                                            default:
                                                echo 'no enviada';
                                                break;
                                        }
                                        ?>
                                    </td>  

                                    <td class="align_center" width="15%">
                                        <?php echo $alarm['correo_para'] ?>
                                    </td>

                                    <td class="align_center" width="15%">
                                        <?php
                                        $mails_count = count(explode(';', $alarm['correo_cc']));
                                        if ($mails_count > 2) {
                                            echo 'Múltiples correos ';
                                            ?>
                                            <a href="#"   title="<?php echo $alarm['correo_cc']; ?>"        >
                                                ( <?php echo (string) $mails_count ?> )
                                            </a>   
                                            <?php
                                        } else {
                                            echo $alarm['correo_cc'];
                                        }
                                        ?>
                                    </td>

                                    <td class="align_center" width="10%">
                                        <button  data-id="<?php echo $alarm['cod_variable']; ?>"
                                                 data-indice="<?php echo $alarm['indice_alarma']; ?>"
                                                 data-tipo="<?php echo $alarm['tipo']; ?>"
                                                 class="editar_alarma btn btn-mini btn-primary" 
                                                 data-url="../alarmas/html/formulario/editar_alarma.php"
                                                 data-usuario="<?php echo $usuario; ?>">
                                            Editar
                                        </button>
                                    </td>

                                    <td class="align_center" width="10%">
                                        <button  data-id="<?php echo $alarm['cod_variable']; ?>"
                                                 data-indice="<?php echo $alarm['indice_alarma']; ?>"
                                                 data-tipo="<?php echo $alarm['tipo']; ?>"
                                                 class="borrar_alarma btn btn-mini btn-danger" 
                                                 data-url="../includes/alarms.php?mod=borrar" >                                           
                                            Eliminar
                                        </button>
                                    </td>

                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>


            </div>            
        </div>
        <div id="modal_editar_alarma" title="Editar alarma" 
             class ="hide" style="height: 100%; width: 500px" >
            <h3 class="center"> Cargando...</h3>
        </div>
        <div id="modal_nueva_alarma" title="Nueva alarma" 
             class ="hide" style="height: 100%; width: 500px" >
            <h3 class="center"> Cargando...</h3>
        </div>

        <script type="text/javascript" language="javascript" src="../assets/js/jquery/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" language="javascript" src="../assets/js/jquery/jquery-ui-1.9.2.custom.min.js"></script>
        <script type="text/javascript" language="javascript" src="../assets/js/jquery/form.min.js"></script>

        <script type="text/javascript" language="javascript" src="../assets/js/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" language="javascript" src="../assets/js/myjs.js"></script>
        <script type="text/javascript" language="javascript" src="../assets/js/time.js"></script>
    </body>
</html>