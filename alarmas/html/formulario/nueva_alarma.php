<?php
require_once "../../../includes/db_pgsql.php";

$cod_variable = isset($_POST['cod_estacion']) ? rawurldecode($_POST['cod_estacion']) : '';
$tipo_estacion = isset($_POST['tipo_estacion']) ? rawurldecode($_POST['tipo_estacion']) : 'CR';
$usuario = isset($_POST['usuario']) ? rawurldecode($_POST['usuario']) : 'jjfernandez';

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

$query_variables = "SELECT * FROM tbl_alarmas_variables 
                WHERE tipo_estacion='$tipo_estacion'
                            ORDER BY variable desc";
$result_query_variables = $db_pgsql->query($query_variables);
$variables_array = $db_pgsql->get_result_array($result_query_variables);
?>
<div class="formulario">
    <div   style="height: 70px;"  >
        <div  class="div_informacio_modal alert hide "></div>
    </div> 
    <form method="POST" name="form-nuevo-dispositivo" class="form-horizontal form_ajax" 
          action="../includes/alarms.php?mod=nueva" >
        <input type="hidden" name="usuario" value="<?php echo $usuario ?>" />
        <input type="hidden" name="tipo_estacion" value="<?php echo $tipo_estacion ?>" />

        <div class="control-group">
            <label class="control-label" for="cod_variable">
                <span class="obligatori">*</span>Código variable
            </label>
            <div class="controls">
                <input class="input-small" type="text" name="cod_variable[]"   maxlength="50" readonly="readonly"
                       placeholder="Identificació" value="<?php echo $cod_variable ?>">
                <select name="cod_variable[]" class="span2">
                    <?php foreach ($variables_array as $variable) { ?>
                        <option value="<?php echo $variable['variable'] ?>"  >
                            <?php echo $variable['variable'] . ' (' . $variable['unidad'] . ')' ?>
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label">
                <span class="obligatori">*</span>Tipo
            </label>
            <div class="controls">
                <select name="tipo">
                    <option value="superior" >Superior</option>
                    <option value="inferior"  >Inferior</option>
                </select>
            </div>
        </div>   

        <div class="control-group">
            <label class="control-label">
                <span class="obligatori">*</span>Umbral
            </label>
            <div class="controls">              
                <input type="text" name="umbral" placeholder="Umbral" maxlength="9"
                       value="">
            </div>
        </div>   

        <div class="control-group">
            <label class="control-label" >
                Activo
            </label>
            <div class="controls">              
                <label class="checkbox">                       
                    <input type="checkbox" name="habilitada" checked="checked" > Habilitar
                </label>
            </div>
        </div>

        <h5 style="text-align: center">Correos</h5>

        <div class="control-group">
            <label class="control-label" >
                <span class="obligatori">*</span>Para:
            </label>
            <div class="controls">              
                <input type="text" name="correo_para" placeholder="Para"
                       value="">
            </div>
        </div>

        <div class="control-group">
            <label class="control-label" >
                <span class="obligatori">*</span>CC:
            </label>
            <div class="controls">              
                <input type="text" name="correo_cc" placeholder="CC"
                       value="">
            </div>
        </div>

        <div class="control-group">
            <div class="controls">               
                <button type="submit" class="btn btn-primary">
                    Crear
                </button>                
            </div>
        </div>

    </form>  
</div>