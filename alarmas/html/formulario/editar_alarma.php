<?php
require_once "../../../includes/db_pgsql.php";
require_once "../../../includes/util.php";

$cod_variable = isset($_POST['cod_variable']) ? rawurldecode($_POST['cod_variable']) : '';

$usuario = isset($_POST['usuario']) ? rawurldecode($_POST['usuario']) : '';

$indice_alarma = isset($_POST['indice_alarma']) ? rawurldecode($_POST['indice_alarma']) : '';
$tipo = isset($_POST['tipo']) ? rawurldecode($_POST['tipo']) : '';

$db_pgsql = new db_pgsql;
$db_pgsql->connect(); // conexión al schema en config.ini => db_schema.

$query_alarms = "SELECT * FROM tbl_alarmas 
                   WHERE cod_variable='$cod_variable' 
                       AND usuario ='$usuario' AND indice_alarma= '$indice_alarma'
                       AND tipo = '$tipo'
                       ORDER BY  cod_variable desc";
$result_query_alarms = $db_pgsql->query($query_alarms);
$alarms_array = $db_pgsql->get_result_array($result_query_alarms);


if (is_array($alarms_array) AND count($alarms_array) === 1) {
    $alarma = $alarms_array[0];
    ?>
    <div class="formulario">
        <div   style="height: 70px;"  >
            <div  class="div_informacio_modal alert hide "></div>
        </div> 
        <form method="POST" name="form-nuevo-dispositivo" class="form-horizontal form_ajax" 
              action="../includes/alarms.php?mod=editar" >
            <input type="hidden" name="usuario" value="<?php echo $usuario ?>" />
            <input type="hidden" name="indice_alarma" value="<?php echo $indice_alarma ?>" />

            <div class="control-group">
                <label class="control-label" for="cod_variable">
                    <span class="obligatori">*</span>Código variable
                </label>
                <div class="controls">
                    <input class="input" type="text" name="cod_variable"   maxlength="50" readonly="readonly"
                           placeholder="Identificació" value="<?php echo $alarma['cod_variable'] ?>">
                   
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" >
                    <span class="obligatori">*</span>Tipo
                </label>
                <div class="controls">              
                    <?php
                    if ($alarma['tipo'] === 'superior') {
                        $superior = 'selected="selected"';
                        $inferior = '';
                    } elseif ($alarma['tipo'] === 'inferior') {
                        $inferior = 'selected="selected"';
                        $superior = '';
                    }
                    ?>
                    <select name="tipo">
                        <option value="superior" <?php echo $superior; ?> >Superior</option>
                        <option value="inferior" <?php echo $inferior; ?>  >Inferior</option>
                    </select>
                </div>
            </div>   

            <div class="control-group">
                <label class="control-label" >
                    <span class="obligatori">*</span>Umbral
                </label>
                <div class="controls">              
                    <input type="text" name="umbral" placeholder="Umbral" maxlength="9"
                           value="<?php echo round_number($alarma['umbral']); ?>">
                </div>
            </div>   

            <div class="control-group">
                <label class="control-label" >
                    Activo
                </label>
                <div class="controls">              
                    <label class="checkbox">
                        <?php $check = $alarma['habilitada'] === 't' ? 'checked="checked"' : ''; ?>
                        <input type="checkbox" name="habilitada"<?php echo $check; ?> > Habilitar
                    </label>
                </div>
            </div>

            <h5 style="text-align: center">Correos</h5>

            <div class="control-group">
                <label class="control-label" >
                    <span class="obligatori">*</span>Para:
                </label>
                <div class="controls">              
                    <input type="text" name="correo_para" placeholder="Umbral"
                           value="<?php echo $alarma['correo_para']; ?>">
                </div>
            </div>

            <div class="control-group">
                <label class="control-label" >
                    <span class="obligatori">*</span>CC:
                </label>
                <div class="controls">              
                    <input type="text" name="correo_cc" placeholder="CC"
                           value="<?php echo $alarma['correo_cc']; ?>">
                </div>
            </div>

            <div class="control-group">
                <div class="controls">               
                    <button type="submit" class="btn btn-primary">
                        Modificar
                    </button>                
                </div>
            </div>

        </form>  
    </div>
<?php } ?>