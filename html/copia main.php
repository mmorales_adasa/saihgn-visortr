<?php
date_default_timezone_set('Europe/Madrid');
//error_reporting(E_ALL);   // VISUALIZA TODOS LOS ERRORES
//error_reporting(0);       // Desactivar toda notificación de error
//
//INCLUDES
include "./login/login.php";
include "./includes/AppRTAPServlet.php";
require_once "./includes/db_pgsql.php";
include "./includes/util.php";


$show_historical = array('em' => TRUE, 'cr' => TRUE, 'can' => TRUE, 'cau' => TRUE);
$show_alarms = array('em' => TRUE, 'cr' => TRUE, 'can' => TRUE, 'cau' => FALSE);


$request = new AppRTAPServlet("http://172.21.0.4:2222/RTAPiWS/GetVariablesValues");
$estaciones = $request->get_xml('xmls/datos_estaciones.xml');

if (!empty($estaciones)) {
    $usuario = isset($_SESSION['usuario']) ? $_SESSION['usuario'] : '';

    $db_pgsql = new db_pgsql;
    $db_pgsql->connect(); // conexión al schema en config.ini => db_schema.
    $query_alarmas = "SELECT * FROM tbl_alarmas WHERE habilitada AND usuario='$usuario' AND enviada =1
                ORDER BY cod_variable desc";
    $result_query_alarmas = $db_pgsql->query($query_alarmas);
    $alarmas_array = $db_pgsql->get_result_array($result_query_alarmas);


    foreach ($alarmas_array as $alarma) {
        $estacion_array = explode('/', $alarma['cod_variable']);
        $estacion = isset($estacion_array[0]) ? $estacion_array[0] : '';

        if ($alarma['tipo'] === 'inferior' AND !empty($estacion)) {
            $colorear[$estacion] = ' background-color: #FFE467; '; // amarillo ocre
        } else if ($alarma['tipo'] === 'superior' AND !empty($estacion)) {
            $colorear[$estacion] = ' background-color: #00A2FF; '; // azul
        }
    }
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>SAIH Guadiana - Consulta de estaciones</title>
        <link rel="Shortcut Icon" href="./assets/img/favicon.ico"></link>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
       <!--<meta name="viewport" content="width=device-width,initial-scale=1.0">-->
        <!--<link rel="stylesheet" href="assets/css/bootstrap.min.css" type="text/css"></link>-->
        <!--<link rel="stylesheet" href="assets/css/bootstrap-responsive.min.css" type="text/css"></link>-->
        <link rel="stylesheet" href="assets/css/style.css" type="text/css"></link>
    </head>
    <body>
        <div id="header-wrap">
            <!--CABECERA-->
            <div class="list header">
                <table class="">
                    <tr>
                        <td><img  src="assets/img/CHG_AgriculturaAlimentaci¢nYMedioAmbiente.png" height="80" width="470"></img></td>
                        <td><img  src="assets/img/logo.png" height="68" width="145"></img></td>
                        <td>
                            <table class="filtermenu">                               
                                <tr>
                                    <td width="200">&nbsp;IR A:&nbsp;&nbsp;
                                        <select id="combo_tipos" class="riverselect" name="f">
                                            <option>Embalses</option>
                                            <option>Caudales </option>
                                            <option>Canales</option>
                                            <option>Cauces</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr class="empty"/>
                                <tr>
                                    <td width="200">Autorefrescará en: </td>
                                    <td><span id="date">0:15:00</span></td>
                                </tr>
                                <tr class="empty"/>
                                <tr>
                                    <td width="200"> Recargar Página</td>
                                    <td width="32"><a href="./index.php"><img class="icon" src="assets/img/reload_icon.png"/></a></td>
                                </tr>
                                <tr></tr>
                            </table>			
                        </td>

                        <td>
                            <table class="updatemenu">
                                <tr class="empty"/>
                                <tr>                                    
                                    <td class="advice" style="text-align: left;"> 
                                        Datos sujetos a validación
                                    </td>
                                </tr>
                                <tr class="empty"/>
                                <tr class="empty"/>			
                                <tr>                                   
                                    <td >Actualizado el:</td>
                                </tr>
                                <tr class="empty"/>
                                <tr>                                   
                                    <td  >
                                        <?php
                                        echo date('d/m/Y H:i');
                                        ?>                                    
                                    </td>   
                                </tr>
                                <tr class="empty">  </tr>                             
                            </table>
                        </td>

                    </tr>                    
                </table>
            </div>
        </div>
        <!--FIN CABECERA-->
        <!--TABLAS-->
        <div id="container">   
            <div class="list">         
                <?php include "tablas/embalses.php"; ?>
            </div>
            <div class="list">
                <?php include "tablas/caudales.php"; ?>
            </div>
            <div class="list">
                <?php include "tablas/canales.php"; ?>
            </div>  
            <div class="list"> 
                <?php include "tablas/controles_rio.php"; ?>
            </div> 
        </div>
    </body>
</body>
<footer>
    <script type="text/javascript" language="javascript" src="./assets/js/jquery/jquery-1.9.1.min.js"></script>
    <script type="text/javascript" language="javascript" src="./assets/js/myjs.js"></script>
    <script type="text/javascript" language="javascript" src="./assets/js/time.js"></script>
</footer>
</html>