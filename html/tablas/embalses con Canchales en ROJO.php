<div class="table_wrapper">              
    <table id="tabla_Embalses" class="list" >
        <thead>
        <th colspan="6"><h2 style="margin: 10px">EMBALSES</h2></th>
        <th colspan="4">TIEMPO REAL</th>
        <th colspan="2">GRÁFICAS</th>
        <th colspan="1">ALARMAS</th>
        </thead>
        <thead>

        <th class="sub" width="5%">Est. Com.</th>
        <th class="sub" width="5%">Código</th>
        <th class="sub" width="10%">Descripción</th>
        <th class="sub" width="15%">Instante Medición</th>        
        <th class="sub" width="10%">Capacidad (hm<sup>3</sup>)</th>
        <th class="sub" width="5%">NMN (msnm)</th>

        <th class="sub" width="10%">Nivel Actual (msnm)</th>
        <th class="sub" width="7%">Diferencia NMN (m)</th>
        <th class="sub" width="8%" height="45px">Volumen Actual (hm<sup>3</sup>)</th>
        <th class="sub" width="10%">% Volumen</th>

        <th class="sub" width="5%">T.R.</th>
        <th class="sub" width="5%">Hist.</th>
        <th class="sub" width="5%">Alarmas</th>

        </thead>
    </table>                  
    <div class="tbody">
        <table class="list" >
            <tbody>
                <?php
                if (isset($estaciones['E'])) {
                    $sort_E = $estaciones['E'];
                    usort($sort_E, array($request, 'myCompare')); //ordeanmos                        
                    foreach ($sort_E as $estacion) {
                        
                        $colorea = isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                        ?>
                        <tr style="height: 23px; <?php echo $colorea; ?>">
                            <!-- EMBALSE-->
                            <td class="align_center" width="5%">
                                <?php

                                echo isset($estacion['Estado_com_gis']) ?
                                        alarm_button($estacion['Estado_com_gis']) : ""; ?>
                            </td>    

                            <td class="align_center" width="5%">
                                <?php echo isset($estacion['label']) ? $estacion['label'] : ""; ?>
                            </td>  

                            <td class="align_center" width="10%">
                                <?php echo isset($estacion['descripcion']) ? $estacion['descripcion'] : "";
								switch ($estacion['label']) {
								case 'E2-04': ?>
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=1" target="_blank" > Ver </a>
								<?php break;
								case 'E2-07': ?>
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=2" target="_blank" >Ver</a>&nbsp;
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=3" target="_blank" >Ver</a>
								<?php break;
								case 'E2-16': ?>
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=5" target="_blank" > Ver </a>
<!--										<a href="https://saihguadiana.com:32554/zm/index.php?view=watch&mid=13" target="_blank" > Ver </a> -->
								<?php break;								
								case 'E2-19': ?>
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=4" target="_blank" > Ver </a>
<!--										<a href="https://saihguadiana.com:32555/zm/index.php?view=watch&mid=12" target="_blank" > Ver </a> -->
								<?php break;
								case 'E2-24': ?>
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=21" target="_blank" > Ver </a>
<!--										<a href="http://saihguadiana.com:32555/zm/index.php?view=watch&mid=12" target="_blank" > Ver </a> -->
								<?php break;								
								case 'E2-32': ?>
									<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=22" target="_blank" > Ver </a>
<!--										<a href="http://saihguadiana.com:32555/zm/index.php?view=watch&mid=12" target="_blank" > Ver </a> -->
								<?php break;
								} ?>
                            </td>   

                            <td class="" width="15%"> <!-- Instante Medición -->
                                <?php
                                if (isset($estacion['Ultima_com'])) {
                                    $show = check_date($estacion['Ultima_com'], "01-ene-70 00H00' UTC");
                                    if ($show) {
                                        echo $estacion['Ultima_com'];
                                    }
                                } else {
                                    $show = FALSE;
                                }
                                ?>
                            </td>     

                            <td class="align_right" width="10%">
                                <?php
                                echo isset($estacion['EMB.Capacidad']) ?
                                        round_number($estacion['EMB.Capacidad']) : "";
                                ?>
                            </td> 

                            <td class="align_right" width="5%"> <!-- Nivel M. (msnm)-->
                                <?php
                                echo isset($estacion['EMB.Cota maxima']) ?
                                        round_number($estacion['EMB.Cota maxima']) : "--";
                                ?>
                            </td> 
                            <!--FIN EMBALSE-->
<?php if ($estacion['label'] == 'E2-16' OR $estacion['label'] == 'E2-22') {	?> <!--es EMBALSE EN ROJO-->
<!--TIMEPO REAL-->
<td class="align_right" width="10%" bgcolor="red"> <!-- NMN (msnm)-->
</td>     

<td class="align_right" width="7%" bgcolor="red">  <!--DIFERENCIA NMN (m)-->
</td>    

<td class="align_right" width="8%" bgcolor="red"> <!--Volumen Actual (hm3)-->
</td>    

<td class="align_right" width="10%" bgcolor="red"><!--% Volumen-->
</td>    
<!--FIN TIEMPO REAL-->

<!--GRÄFICAS-->
<td width="5%">
</td>

<td width="5%">
</td>
<!--FIN GRÄFICAS-->
<?php }
else
{ ?> <!--no es EMBALSE EN ROJO-->
                            <!--TIMEPO REAL-->
                            <td class="align_right" width="10%"> <!-- NMN (msnm)-->
                                <?php
                                if ($show) {
                                    echo isset($estacion['EMB.Nivel']) ?
                                            round_number($estacion['EMB.Nivel']) : "";
                                }
                                ?>
                            </td>     

                            <td class="align_right" width="7%">  <!--DIFERENCIA NMN (m)-->
                                <?php
                                if ($show) {
                                    echo isset($estacion['EMB.Cota maxima']) ?
                                            round_number($estacion['EMB.Cota maxima'] - $estacion['EMB.Nivel']) : "--";
                                }
                                ?>
                            </td>    

                            <td class="align_right" width="8%"> <!--Volumen Actual (hm3)-->
                                <?php
                                if ($show) {
                                    echo isset($estacion['EMB.Volumen']) ?
                                            round_number($estacion['EMB.Volumen']) : "";
                                }
                                ?>
                            </td>    

                            <td class="align_right" width="10%"><!--% Volumen-->
                                <?php
                                if ($show) {
                                    echo isset($estacion['EMB.Porcentaje_v']) ?
                                            round_number($estacion['EMB.Porcentaje_v']) : "";
                                }
                                ?>
                            </td>    
                            <!--FIN TIEMPO REAL-->

                            <!--GRÄFICAS-->
                            <td width="5%">
                                <?php
                                $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                                $show_graph_week = check_date_week($estacion['Ultima_com']);
                                if ($show AND $show_graph_week) {
                                    ?>
                                    <a href="./charts/charts_tr_em.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
                                       target="_blank" >
                                        TR
                                    </a>
                                <?php }; ?>
                            </td>

                            <td width="5%">
                                <?php if (($show OR $show_graph_week) AND $show_historical['em']) { ?>
                                    <a href="./charts/charts_his_em.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
                                       target="_blank" >
                                        HIST
                                    </a>
                                <?php } ?>
                            </td>
                            <!--FIN GRÄFICAS-->
<?php } ?> <!--FIN no es EMBALSE EN ROJO-->
                            <td width="5%"><!--ALARMAS-->
                                <?php if ($show_alarms['em']) { ?>
                                    <a href="./alarmas/?cod_estacion=<?php echo $station ?>&tipo_estacion=E&descripcion=<?php echo $estacion['descripcion']; ?>"
                                       target="_blank" >
                                        Alarmas
                                    </a>                                        
                                <?php } ?>
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
