<div class="table_wrapper">
    <table id="tabla_CotasAbsolRios" class="list">
        <thead>
        <th colspan="3"> <h2 style="margin: 10px">COTAS ABSOLUTAS EN RÍO</h2> </th>
        <th colspan="1">TIEMPO REAL</th>                         
        <th colspan="2">GRÁFICAS</th>                        
        </thead>
        <thead>
        <th class="sub" width="10%">Código</th>     
        <th class="sub" width="25%">Descripción</th>
        <th class="sub" width="20%">Instante Medición</th>
        <th class="sub" width="15%">Cota Absoluta en Río (msnm)</th>

        <th class="sub" width="10%">T.R.</th>
        <th class="sub" width="10%">Hist.</th>
        </thead>
    </table>                
	
    <div class="tbody">
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
                <?php
                if (isset($estaciones['NRA'])) {
                    $matriz_NRA = $estaciones['NRA'];

                    $matriz_to_sort = array('CR2-01','CR2-06','CR2-13','CR2-51','CR2-19','NR2-35','CR2-20','CR2-21','CR2-23','CR2-25','CR2-31','CR2-34','CR2-48','NR2-36','CR2-45','CR2-49','E2-25','CR2-55');

                    $matriz_NRA = sortArrayByArray($matriz_NRA, $matriz_to_sort);
                    foreach ($matriz_NRA as $estacion) {

                        $colorea = isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                        ?>
                        <tr style="height: 23px; <?php echo $colorea; ?>">  
							<td class="align_center" width="10%">
								<?php
								$parameter = 'NRA';
								echo isset($estacion['label']) ?
										$estacion['label'] . '/' . $parameter : "";
								?>
							</td>  						
                            <td class="align_left" width="25%">
                                <?php echo isset($estacion['descripcion']) ? $estacion['descripcion'] : ""; ?>
                            </td>           

                            <td class="" width="20%">                                               
                                <?php
                                if (isset($estacion['Ultima_com'])) {
                                    $show = check_date($estacion['Ultima_com'], "01-ene-70 00H00' UTC");
                                    if ($show) {
                                        echo $estacion['Ultima_com'];
                                    }
                                } else {
                                    $show = FALSE;
                                }
                                ?>                                                
                            </td>            
                            <td class="align_right" width="15%">
                                <?php
                                if ($show) {
                                    echo isset($estacion['NRA.Nivel']) ?
                                            round_number($estacion['NRA.Nivel']) : "";
                                }
                                ?>
                            <td width="10%">
                                <?php
                                $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                                $show_graph_week = check_date_week($estacion['Ultima_com']);
								
                                if ($show AND $show_graph_week) {
                                    ?>
                                    <a href="./charts/charts_tr_nra.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
                                       target="_blank" >
                                        TR
                                    </a>
                                <?php }; ?>
                            </td>

                            <td width="10%"><!--HIST-->
                                <?php if (($show OR $show_graph_week) AND $show_historical['cr']) { 
									?>
                                    <a href="./charts/charts_his_nra.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['descripcion']; ?>"
                                       target="_blank" >
                                        HIST
                                    </a>
                                <?php } ?>
                            </td>
                        </tr>
                        <?php
                    }
				}
                ?>
            </tbody>
        </table>
    </div>
</div>