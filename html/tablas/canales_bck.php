<div class="table_wrapper">
    <table id="tabla_Canales" class="list">
        <thead>
        <th colspan="3"> <h2 style="margin: 10px">CANALES</h2> </th>
        <th colspan="1">TIEMPO REAL</th>                         
        <th colspan="2">GRÁFICAS</th>                        
        </thead>
        <thead>
        <th class="sub" width="10%">Código</th>     
        <th class="sub" width="25%">Descripción</th>
        <th class="sub" width="20%">Instante Medición</th>
        <th class="sub" width="15%">Caudal (m3/s)</th>

        <th class="sub" width="10%">T.R.</th>
        <th class="sub" width="10%">Hist.</th>

        </thead>
    </table>                
    <div class="tbody" style="width: 96%; height: 313px" > <!--  -->
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
                <?php
                if (isset($estaciones['Canal'])) {
                    $sort_C = $estaciones['Canal'];

                    $arra_to_sort = array('E2-03', 'E2-12', 'E2-13', 'E2-10', 'E2-04', 'E2-11', 'E2-34',
                        'E2-07', 'E2-19', 'E2-19');

//   usort($estacion['canales'], array($request, 'myCompare')); //ordeanmos

                    $sort_C_2 = sortArrayByArray($sort_C, $arra_to_sort);
                    foreach ($sort_C_2 as $estacion) {

                        $sort_Canales = $estacion['canales'];

                        foreach ($sort_Canales as $key => $value) {
                            if (!is_array($value)) {

                                $colorea = ""; // No Coloreas//  isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                                ?>
                                <tr style="height: 23px; <?php echo $colorea; ?>">  
                                    <!-- CANALES-->
                                    <td class="align_center" width="10%">
                                        <?php
                                        $parameter = substr($key, 0, -7);
                                        echo isset($estacion['label']) ?
                                                $estacion['label'] . '/' . $parameter : "";
                                        ?>
                                    </td>  

                                    <td class="align_center" width="25%">
                                        <?php echo $estacion['canales'][$parameter]['descripcion_canal'];
										switch ($estacion['label']) {
											case 'E2-03': ?>
												<a href="/vistr/html/GSola_Canal.php" target="_blank" > Ver </a>
											<?php break;
											case 'E2-04':
												if ($parameter == 'CA1') { ?>											
												<a href="/vistr/html/Orellana_Canal.php" target="_blank" > Ver </a>
											<?php } break;											
											case 'E2-07':
												if ($parameter == 'CA1') { ?>																						
												<a href="/vistr/html/Zujar_Canal.php" target="_blank" > Ver </a>
											<?php } break;												
											case 'E2-19':
												if ($parameter == 'QC2') { ?>
													<a href="/vistr/html/CamaraCLobon.php" target="_blank" > Ver </a>
											<?php }
												else { ?>
													<a href="/vistr/html/CamaraCMontijo.php" target="_blank" > Ver </a>											
											<?php  } break;																							
										} ?>
                                    </td>     

                                    <td class="align_center" width="20%">
                                        <?php
                                        echo $estaciones['E'][$estacion['label']]['Ultima_com'];
                                        ?>
                                    </td>

                                    <!-- TIEMPO REAL-->
									<?php if ($estacion['label'] == 'E2-19') {
									?>
									<td class="align_right" width="15%" bgcolor="red">
									<?php }
									else
									{
                                    ?>
									<td class="align_right" width="15%">
									<?php echo round_number($value);
									}
                                    ?>
                                    </td>  

                                    <!-- GRÄFICAS-->
                                    <td width="10%">
                                    <?php
									if ($estacion['label'] == 'E2-19') {
									}
									else {
									    $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                                        $show_graph_week = check_date_week($estaciones['E'][$estacion['label']]['Ultima_com']);
                                        if ($show AND $show_graph_week) {
                                            ?>
                                            <a href="./charts/charts_tr_ca.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['canales'][$parameter]['descripcion_canal'] ?>" 
                                               target="_blank" >
                                                TR
                                            </a>
                                        <?php }; }?>
                                    </td>

                                    <td width="10%"><!--HIST-->
                                    <?php
									if ($estacion['label'] == 'E2-19') {
									}
									else {
										if (($show OR $show_graph_week) AND $show_historical['can']) { ?>
											<a href="./charts/charts_his_ca.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['canales'][$parameter]['descripcion_canal'] ?>"
											   target="_blank" >
												HIST
											</a>                                        
                                        <?php } }?>
                                    </td>

                                    <!-- FIN GRÄFICAS-->
                                </tr>
                                <?php
                            }
                        }
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>