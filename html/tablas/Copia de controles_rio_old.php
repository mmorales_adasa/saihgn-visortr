<div class="table_wrapper">
    <table id="tabla_Cauces" class="list">
        <thead>
        <th colspan="4"><h2 style="margin: 10px">CONTROLES EN RÍO</h2></th>
        <th colspan="2">TIEMPO REAL</th>
        <th colspan="2">GRÁFICAS</th>
        <th colspan="1">ALARMAS</th>
        </thead>
        <thead>
        <th class="sub" width="10%">Est. Comunicación</th>
        <th class="sub" width="10%">Código</th>
        <th class="sub" width="25%">Descripción</th>
        <th class="sub" width="15%">Instante Medición</th>

        <th class="sub" width="10%">Nivel Actual (m)</th>
        <th class="sub" width="10%">Caudal (m3/s)</th>

        <th class="sub" width="5%">T.R.</th>
        <th class="sub" width="5%">Hist.</th>
        <th class="sub" width="10%">Alarmas</th>
        </thead>
    </table>                
    <div class="tbody">
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
                <?php
                if (isset($estaciones['CR'])) {
                    $sort_CR = $estaciones['CR'];
                    usort($sort_CR, array($request, 'myCompare')); //ordeanmos
                    foreach ($sort_CR as $estacion) {

                        $colorea = isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                        ?>
                        <tr style="<?php echo $colorea ;?>"  >
                            <td class="align_center" width="10%">
								<!-- Para aforos con solo nivel ponemos 'parameter'-->
                                <?php
								$parameter = 'NR1';
if ($estacion['label'] <> 'CR1-01') {								
                                echo isset($estacion['Estado_com_gis']) ?
                                        alarm_button($estacion['Estado_com_gis']) : "";
}										
                                ?>
                            </td>            
                            <td class="align_center" width="10%">
                                <?php echo isset($estacion['label']) ? $estacion['label'] : ""; ?>
                            </td>            
                            <td class="align_left" width="25%">
                                <?php echo isset($estacion['descripcion']) ? $estacion['descripcion'] : ""; ?>
                            </td>           

                            <td class="" width="15%">                                               
                                <?php
                                if (isset($estacion['Ultima_com'])) {
                                    $show = check_date($estacion['Ultima_com'], "01-ene-70 00H00' UTC");
                                    if ($show) {
                                        echo $estacion['Ultima_com'];
                                    }
                                } else {
                                    $show = FALSE;
                                }
                                ?>                                                
                            </td>            
<?php if ($estacion['label'] == 'CR1-01') { ?>
<td class="align_right" width="10%" bgcolor="red">
</td>            
<td class="align_right" width="10%" bgcolor="red">
</td>    
<td width="5%">
</td>
<td width="5%"><!--HIST-->
</td>
<?php }
else
{ ?>
                            <td class="align_right" width="10%">
                                <?php
                                if ($show) {
                                    echo isset($estacion['EA.Nivel']) ?
                                            round_number($estacion['EA.Nivel']) : "";
                                }
                                ?>
                            </td>            
                            <td class="align_right" width="10%">
                                <?php
                                if ($show) {
                                    echo isset($estacion['EA.Caudal']) ?
                                            round_number($estacion['EA.Caudal']) : "";
                                }
                                ?>
                            </td>    
                            <td width="5%">
                                <?php
                                $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                                $show_graph_week = check_date_week($estacion['Ultima_com']);
                                if ($show AND $show_graph_week) {
									if ($station == 'CR2-53' OR $station == 'CR2-54') {
										?>
										<a href="./charts/charts_tr_nr.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
										   target="_blank" >
											TR
										</a>										
									<?php }
									else {?>
										<a href="./charts/charts_tr_cr.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
										   target="_blank" >
											TR
										</a>
                                <?php };
								}; ?>
                            </td>

                            <td width="5%"><!--HIST-->
                                <?php
								if (($show OR $show_graph_week) AND $show_historical['cr']) {
									if ($station == 'CR2-53' OR $station == 'CR2-54') { ?>
                                    <a href="./charts/charts_his_nr.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['descripcion']; ?>"									
                                       target="_blank" >
                                        HIST
                                    </a>
									<?php } 
									else {?>
                                    <a href="./charts/charts_his_cr.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
                                       target="_blank" >
                                        HIST
                                    </a>									
									<?php };
								}; ?>								
                            </td> <!--FIN HIST-->
<?php }
?> <!--FIN no es CR1-01-->
                            <td width="10%"><!--ALARMAS-->
                                <?php if ($show_alarms['cr']) { ?>
                                    <a href="./alarmas/?cod_estacion=<?php echo $station ?>&tipo_estacion=CR&descripcion=<?php echo $estacion['descripcion']; ?>"
                                       target="_blank" >
                                        Alarmas
                                    </a>                                        
                                <?php } ?>
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>