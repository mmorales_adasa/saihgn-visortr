
<div class="table_wrapper">
    <table id="tabla_Caudales" class="list">
        <thead>
        <th colspan="3"> <h2 style="margin: 10px">CAUDALES DE SALIDA</h2> </th>
        <th colspan="4">TIEMPO REAL</th>                         
<!--        <th colspan="2">GRÁFICAS</th>                         -->

        </thead>
        <thead>
        <th class="sub" width="10%">Código</th>     
        <th class="sub" width="15%">Descripción</th>
        <th class="sub" width="15%">Instante Medición</th>

        <th class="sub" width="12%">Q Fondo (m3/s)</th>  <!--  (valor de QFT)-->
        <th class="sub" width="23%">Q Turbinado (m3/s)</th><!--  (valor de QWT)-->
        <th class="sub" width="15%">Q Aliviado (m3/s)</th><!--  valor de QAT)-->
        <th class="sub" width="10%">Caudal Total de Salida (m3/s)</th><!--  (suma de los tres anteriores) -->

<!--        <th class="sub" width="10%">T.R.</th>
        <th class="sub" width="10%">Hist.</th>-->
        </thead>
    </table>                
    <div class="tbody"  > <!-- style="width: 96%;" -->
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
                <?php
                if (isset($estaciones['Caudal'])) {
                    $sort_C = $estaciones['Caudal'];
                    usort($sort_C, array($request, 'myCompare')); //ordeanmos
                    foreach ($sort_C as $estacion) {


                        $colorea = ""; // No Coloreas// isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                        ?>
                        <tr style="height: 23px; <?php echo $colorea; ?>">
                            <!-- Caudal en río-->

                            <td class="align_center" width="10%">
                                <?php echo isset($estacion['label']) ? $estacion['label'] : ""; ?>
                            </td>  

                            <td class="align_center" width="15%">
                                <?php echo isset($estacion['descripcion']) ? $estacion['descripcion'] : ""; ?>
                            </td>   

                            <td class="" width="15%"> <!-- Instante Medición -->
                                <?php
                                if (isset($estacion['Ultima_com'])) {
                                    $show = check_date($estacion['Ultima_com'], "01-ene-70 00H00' UTC");
                                    if ($show) {
                                        echo $estacion['Ultima_com'];
                                    }
                                } else {
                                    $show = FALSE;
                                }
                                ?>
                            </td>     
                            <!--INICIO TIEMPO REAL-->     
                            <td class="align_right" width="12%">
                                <?php if (isset($estacion['QFT.Caudal'])) { ?>

                                    <span><?php echo round_number($estacion['QFT.Caudal']); ?></span>
                                    (<?php
                        $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                        $show_graph_week = check_date_week($estacion['Ultima_com']);
                        if ($show AND $show_graph_week) {
                                        ?>
                                        <a href="./charts/charts_tr_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QFT.Caudal" 
                                           target="_blank" >
                                            TR
                                        </a>
                                    <?php }; ?>)

                                    (<?php if (($show OR $show_graph_week) AND $show_historical['em']) { ?>
                                        <a href="./charts/charts_his_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QFT.Caudal" 
                                           target="_blank" >
                                            HIST
                                        </a>
                                    <?php } ?>)
                                <?php } ?>
                            </td> 
                            <td class="align_right" width="23%">
                                <?php if (isset($estacion['QWC.Caudal']) AND isset($estacion['QWR.Caudal'])) { ?>
                                   QWR:<?php echo round_number($estacion['QWR.Caudal']); ?> | QWC:<?php echo round_number($estacion['QWC.Caudal']); ?>
                                    (<?php
                        $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                        $show_graph_week = check_date_week($estacion['Ultima_com']);
                        if ($show AND $show_graph_week) {
                                        ?>
                                        <a href="./charts/charts_tr_cau_2.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QWC.Caudal&parameter_2=QWR.Caudal" 
                                           target="_blank" >
                                            TR
                                        </a>
                                    <?php } ?>)
                                    (<?php if (($show OR $show_graph_week) AND $show_historical['em']) { ?>
                                        <a href="./charts/charts_his_cau_2.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QWC&parameter_2=QWR" 
                                           target="_blank" >
                                            HIST
                                        </a>)
                                    <?php } ?>

                                <?php } elseif (isset($estacion['QWT.Caudal'])) { ?>
                                    <span> <?php echo round_number($estacion['QWT.Caudal']); ?></span>
                                    (<?php
                        $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                        $show_graph_week = check_date_week($estacion['Ultima_com']);
                        if ($show AND $show_graph_week) {
                                        ?>
                                        <a href="./charts/charts_tr_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QWT.Caudal" 
                                           target="_blank" >
                                            TR
                                        </a>
                                    <?php } ?>)
                                    (<?php if (($show OR $show_graph_week) AND $show_historical['em']) { ?>
                                        <a href="./charts/charts_his_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QWT.Caudal" 
                                           target="_blank" >
                                            HIST
                                        </a>)
                                    <?php } ?>
                                <?php } elseif (isset($estacion['QWR.Caudal']) AND !isset($estacion['QWC.Caudal'])) { ?>
                                    <span> QWR:<?php echo round_number($estacion['QWR.Caudal']); ?></span>
                                    (<?php
                        $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                        $show_graph_week = check_date_week($estacion['Ultima_com']);
                        if ($show AND $show_graph_week) {
                                        ?>
                                        <a href="./charts/charts_tr_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QWR.Caudal" 
                                           target="_blank" >
                                            TR
                                        </a>
                                    <?php } ?>)
                                    (<?php if (($show OR $show_graph_week) AND $show_historical['em']) { ?>
                                        <a href="./charts/charts_his_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QWR.Caudal" 
                                           target="_blank" >
                                            HIST
                                        </a>)
                                    <?php } ?>
                                <?php } ?>
                            </td> 
                            <td class="align_right" width="15%">
        <?php if (isset($estacion['QAT.Caudal'])) { ?>
                                    <span><?php echo round_number($estacion['QAT.Caudal']); ?></span>

                                    (<?php
            $station = isset($estacion['label']) ? trim($estacion['label']) : "";
            $show_graph_week = check_date_week($estacion['Ultima_com']);
            if ($show AND $show_graph_week) {
                ?>
                                        <a href="./charts/charts_tr_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QAT.Caudal" 
                                           target="_blank" >
                                            TR
                                        </a>
            <?php }; ?>)

                                    (<?php if (($show OR $show_graph_week) AND $show_historical['em']) { ?>
                                        <a href="./charts/charts_his_cau.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>&parameter_1=QAT.Caudal" 
                                           target="_blank" >
                                            HIST
                                        </a>
                                    <?php } ?>)
        <?php }; ?>
                            </td> 

                            <td class="align_right" width="10%">
                                <?php
                                $total = isset($estacion['QFT.Caudal']) ? $estacion['QFT.Caudal'] : 0;
                                $total += isset($estacion['QAT.Caudal']) ? $estacion['QAT.Caudal'] : 0;
								if (isset($estacion['QWC.Caudal']) OR isset($estacion['QWR.Caudal'])) {
								  $total += isset($estacion['QWR.Caudal']) ? $estacion['QWR.Caudal'] : 0;
								  $total += isset($estacion['QWC.Caudal']) ? $estacion['QWC.Caudal'] : 0;
								}
								else {
								  $total += isset($estacion['QWT.Caudal']) ? $estacion['QWT.Caudal'] : 0;								
								};
                                echo round_number($total);
                                ?>
                            </td> 
                            <!--FIN TIEMPO REAL-->                        
                        </tr> 
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>