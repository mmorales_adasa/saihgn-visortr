<div class="table_wrapper">
    <table id="tabla_Canales" class="list">
        <thead>
        <th colspan="3"> <h2 style="margin: 10px">CANALES</h2> </th>
        <th colspan="1">TIEMPO REAL</th>                         
        <th colspan="2">GRÁFICAS</th>                        
        </thead>
        <thead>
        <th class="sub" width="10%">Código</th>     
        <th class="sub" width="25%">Descripción</th>
        <th class="sub" width="20%">Instante Medición</th>
        <th class="sub" width="15%" height="30px">Caudal (m<sup>3</sup>/s)</th>

        <th class="sub" width="10%">T.R.</th>
        <th class="sub" width="10%">Hist.</th>

        </thead>
    </table>                
    <div class="tbody" style="width: 96%; height: 350px" > <!--  -->
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
				<tr style="padding=5px;" align='center'>
					<td style="padding-top: 5px; padding-right: 5px; padding-bottom: 5px;">E1-01/CA1</td><td>Toma de canal de Peñarroya</td>
					<td><?php echo $estaciones['E']['E1-01']['Ultima_com']?></td><td style="padding-right: 5px;" align='right'><?php echo $estaciones['E']['E1-01']['CA1.Caudal']?></td>
					<td align='left'><a href="./charts/charts_tr_ca.php?station=E1-01&parameter=CA1&label=Toma de canal de Peñarroya" target="_blank" >TR</a></td>
					<td align='left'><a href="./charts/charts_his_ca.php?station=E1-01&parameter=CA1&label=Toma de canal de Peñarroya"
                                               target="_blank" >
                                                HIST
                                            </a> </td>
				</tr>
                <?php
                if (isset($estaciones['Canal'])) {
                    $sort_C = $estaciones['Canal'];
					//print_r($estaciones);
                    $arra_to_sort = array('E2-03', 'E2-12', 'E2-13', 'E2-10', 'E2-04', 'E2-11', 'E2-34',
                        'E2-07', 'E2-19', 'E2-19','E1-01');

//  usort($estacion['canales'], array($request, 'myCompare')); //ordeanmos

                    $sort_C_2 = sortArrayByArray($sort_C, $arra_to_sort);
				//print_r( $sort_C_2)	;
                    foreach ($sort_C_2 as $estacion) {
						$sort_array= array('CA1','QC1','QC2','QC3','QTR');
                        $sort_Canales = $estacion['canales'];
						ksort( $sort_Canales);
//print_r( $estacion['canales'] );
                        foreach ($sort_Canales as $key => $value) {
                            if (!is_array($value)) {

                                $colorea = ""; // No Coloreas//  isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                                ?>
                                <tr style="height: 23px; <?php echo $colorea; ?>">  
                                    <!-- CANALES-->
                                    <td class="align_center" width="10%">
                                        <?php
                                        $parameter = substr($key, 0, -7);
                                        echo isset($estacion['label']) ?
                                                $estacion['label'] . '/' . $parameter : "";
                                        ?>
                                    </td>  

                                    <td class="align_center" width="25%">
                                        <?php echo $estacion['canales'][$parameter]['descripcion_canal'];
										switch ($estacion['label']) {
											case 'E2-03': ?>
												<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=20" target="_blank" > Ver </a>
											<?php break;
											case 'E2-04':
												if ($parameter == 'CA1') { ?>											
												<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=7" target="_blank" > Ver </a>
											<?php } break;											
											case 'E2-07':
												if ($parameter == 'CA1') {
												$file = fopen("E2-07_Canal.txt", "w");
												fwrite($file, $estacion['Ultima_com'] . PHP_EOL);
												fwrite($file, round_number($value) . PHP_EOL);
												fclose($file); ?>												
												<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=8" target="_blank" > Ver </a>
											<?php } break;												
											case 'E2-19':
												if ($parameter == 'QC2') { ?>
													<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=10" target="_blank" > Ver </a>
											<?php }
												else { ?>
													<a href="https://saihguadiana.com:32555/zm/index.php?action=login&view=postlogin&username=view&password=view&view=watch&mid=9" target="_blank" > Ver </a>											
											<?php  } break;																							
										} ?>
                                    </td>     

                                    <td class="align_center" width="20%">
                                        <?php
										if ($estacion['label'] == 'E2-19') {
										  #$dbconn = pg_connect("host=172.21.0.81 port=5432 dbname=SAIHG user=postgres password=Adasa001");
										  $dbconn = pg_connect("host='172.21.0.235' dbname='saihg' user='saihg' password='saihg' port=5435");
											
										  $variableCanal = $estacion['label'] . '/' . $parameter;
										  $stringQuery = "select valor,timestamp_medicion from tbl_hist_medidas where cod_variable = '" . $variableCanal . "' and ind_ult_valor = 1";
										
										  $result=pg_query($dbconn, $stringQuery);
										  if  (!$result) {
										    echo "query did not execute";
										  }	
										  else {
										    $array_result=pg_fetch_array($result);
											echo date('d-M-y H:m',strtotime($array_result[1])) . ' UTC';
										  }
										}
										else {
                                          echo $estaciones['E'][$estacion['label']]['Ultima_com'];
										}
                                        ?>
                                    </td>

                                    <!-- TIEMPO REAL-->
									<?php
									if ($estacion['label'] == 'E2-19') {
									?>									
									  <td class="align_right" width="15%">
									  <?php echo round_number($array_result[0]);
									}
									else
									{
                                    ?>									
									<td class="align_right" width="15%">
									<?php echo round_number($value);
                                    ?>
                                    </td> 
									
									<?php } ?>
									<td></td>

                                    <!-- GRÄFICAS-->
                                    <td width="10%">
                                    <?php
									    $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                                        $show_graph_week = check_date_week($estaciones['E'][$estacion['label']]['Ultima_com']);
                                        if ($show OR $show_graph_week) {
											if ($estacion['label'] == 'E2-19') {
												?>
												<a href="./charts/charts_tr_ca_remote.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['canales'][$parameter]['descripcion_canal'] ?>" 
												   target="_blank" >
													TR
												</a>
												<?php
											}
											else {
												?>
												<a href="./charts/charts_tr_ca.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['canales'][$parameter]['descripcion_canal'] ?>" 
												   target="_blank" >
													TR
												</a>
												<?php
											}
										}; ?>
                                    </td>

                                    <td width="10%"><!--HIST-->
                                        <?php
										  if (($show OR $show_graph_week) AND $show_historical['can']) { ?>
                                            <a href="./charts/charts_his_ca.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['canales'][$parameter]['descripcion_canal'] ?>"
                                               target="_blank" >
                                                HIST
                                            </a>                                        
                                        <?php } ?>
                                    </td>

                                    <!-- FIN GRÄFICAS-->
                                </tr>

                                <?php
								
                            }
                        }
                    }
                }
                ?>
			
        </table>
    </div>
</div>