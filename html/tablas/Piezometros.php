<div class="table_wrapper">
    <table id="tabla_Piezometros" class="list">
        <thead>
        <th colspan="4"><h2 style="margin: 10px">CONTROLES PIEZOMÉTRICOS</h2></th>
        <th colspan="1">TIEMPO REAL</th>
        <th colspan="1">GRÁFICAS</th>
        <th colspan="1">ALARMAS</th>
        </thead>
        <thead>
        <th class="sub" width="8%">Est. Comunicación</th>
        <th class="sub" width="14%">Código</th>
        <th class="sub" width="34%">Descripción</th>
        <th class="sub" width="16%">Instante Medición</th>

        <th class="sub" width="8%">Cota (m)</th>

        <th class="sub" width="10%">Hist.</th>
        <th class="sub" width="10%">Alarmas</th>
        </thead>
    </table>                
    <div class="tbody">
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
                <?php
                if (isset($estaciones['CP'])) {
                    $sort_CP = $estaciones['CP'];
                    usort($sort_CP, array($request, 'myCompare')); //ordeanmos
                    foreach ($sort_CP as $estacion) {
if ($estacion['label'] <> 'CP1-06') {
                        $colorea = isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                        ?>
                        <tr style="<?php echo $colorea ;?>"  >
                            <td class="align_center" width="8%">
                                <?php
                                echo isset($estacion['Estado_com_gis']) ?
                                        alarm_button($estacion['Estado_com_gis']) : "";
                                ?>
                            </td>            
                            <td class="align_center" width="14%">
                                <?php echo isset($estacion['label']) ? $estacion['label'] : ""; ?>
                            </td>            
                            <td class="align_left" width="34%">
                                <?php echo isset($estacion['descripcion']) ? $estacion['descripcion'] : ""; ?>
                            </td>           

                            <td class="" width="16%">                                               
                                <?php
                                if (isset($estacion['Ultima_com'])) {
                                    $show = check_date($estacion['Ultima_com'], "01-ene-70 00H00' UTC");
                                    if ($show) {
                                        echo $estacion['Ultima_com'];
                                    }
                                } else {
                                    $show = FALSE;
                                }
                                ?>                                                
                            </td>            

                            <td class="align_right" width="8%">
                                <?php
                                if ($show) {
                                    echo isset($estacion['CP.Cota']) ?
                                            round_number($estacion['CP.Cota']) : "";
                                }
                                ?>
                            </td>            

                            <td width="10%"><!--HIST-->
                                <?php if (($show OR $show_graph_week) AND $show_historical['cp']) { 
									$station = isset($estacion['label']) ? trim($estacion['label']) : "";
								?>
                                    <a href="./charts/charts_his_cp.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
                                       target="_blank" >
                                        HIST
                                    </a>
                                <?php } ?>
                            </td>

                            <td width="10%"><!--ALARMAS-->
                                <?php if ($show_alarms['cp']) { ?>
                                    <a href="./alarmas/?cod_estacion=<?php echo $station ?>&tipo_estacion=CP&descripcion=<?php echo $estacion['descripcion']; ?>"
                                       target="_blank" >
                                        Alarmas
                                    </a>                                        
                                <?php } ?>
                            </td>

                        </tr>
                        <?php
} //if no es CP1-06											
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>