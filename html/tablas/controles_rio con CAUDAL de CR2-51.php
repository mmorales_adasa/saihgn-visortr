<div class="table_wrapper">
    <table id="tabla_Cauces" class="list">
        <thead>
        <th colspan="4"><h2 style="margin: 10px">CONTROLES EN RÍO</h2></th>
        <th colspan="2">TIEMPO REAL</th>
        <th colspan="2">GRÁFICAS</th>
        <th colspan="1">ALARMAS</th>
        </thead>
        <thead>
        <th class="sub" width="10%">Est. Comunicación</th>
        <th class="sub" width="10%">Código</th>
        <th class="sub" width="25%">Descripción</th>
        <th class="sub" width="15%">Instante Medición</th>

        <th class="sub" width="10%">Nivel Actual (m)</th>
        <th class="sub" width="10%">Caudal (m3/s)</th>

        <th class="sub" width="5%">T.R.</th>
        <th class="sub" width="5%">Hist.</th>
        <th class="sub" width="10%">Alarmas</th>
        </thead>
    </table>                
    <div class="tbody">
        <table class="list" border="1" cellspacing="0" cellpadding="0" width="95%">
            <tbody>
                <?php
                if (isset($estaciones['CR'])) {
                    $matriz_CR = $estaciones['CR'];
                    $matriz_to_sort = array('CR1-26','CR1-01','CR1-02','CR1-03','CR1-25','CR1-04','CR1-05','CR1-06','CR1-07','CR1-08','CR1-09','CR1-10','CR1-11','CR1-12','CR1-13','CR1-14','CR1-15','CR1-16','CR1-17','CR1-18','CR1-19','CR1-20','CR1-21','CR1-22','CR1-23','CR1-24','CR2-01','CR2-02','CR2-03','CR2-04','CR2-05','CR2-52','CR2-06','CR2-07','CR2-08','CR2-09','CR2-10','CR2-11','CR2-12','CR2-13','CR2-14','CR2-51','CR2-15','CR2-16','CR2-17','CR2-18','CR2-19','CR2-53','CR2-20','CR2-21','CR2-22','CR2-23','CR2-24','CR2-25','CR2-26','CR2-27','CR2-28','CR2-29','CR2-30','CR2-31','CR2-32','CR2-33','CR2-34','CR2-35','CR2-36','CR2-37','CR2-38','CR2-39','CR2-40','CR2-41','CR2-42','CR2-54','CR2-43','CR2-44','CR2-45','CR2-46','CR2-47','CR2-48','CR2-49','E2-25','CR2-55','CR2-50','CR3-01');
                    $matriz_CR = sortArrayByArray($matriz_CR, $matriz_to_sort);

                    foreach ($matriz_CR as $estacion) {				
                        $colorea = isset($colorear[$estacion['label']]) ? $colorear[$estacion['label']] : '';
                        ?>
                        <tr style="<?php echo $colorea ;?>"  >
                            <td class="align_center" width="10%">
								<!-- Para aforos con solo nivel ponemos 'parameter'-->
                                <?php
								$parameter = 'NR1';
if ($estacion['label'] == 'CR1-01' OR $estacion['label'] == 'CR2-46' OR $estacion['label'] == 'CR2-11' OR $estacion['label'] == 'CR2-14') {
                                    $show = FALSE;
                                } else {
                                echo isset($estacion['Estado_com_gis']) ?
                                        alarm_button($estacion['Estado_com_gis']) : "";
}										
                                ?>
                            </td>            
                            <td class="align_center" width="10%">
                                <?php echo isset($estacion['label']) ? $estacion['label'] : ""; ?>
                            </td>            
                            <td class="align_left" width="25%">
                                <?php echo isset($estacion['descripcion']) ? $estacion['descripcion'] : ""; ?>
                            </td>           

                            <td class="" width="15%">                                               
                                <?php
                                if (isset($estacion['Ultima_com'])) {
                                    $show = check_date($estacion['Ultima_com'], "01-ene-70 00H00' UTC");
                                    if ($show) {
                                        echo $estacion['Ultima_com'];
                                    }									
                                }
                                ?>                                                
                            </td>            
<?php if ($estacion['label'] == 'CR1-01' OR $estacion['label'] == 'CR2-46' OR $estacion['label'] == 'CR2-11' OR $estacion['label'] == 'CR2-14') { ?>
<td class="align_right" width="10%" bgcolor="red">
</td>            
<td class="align_right" width="10%" bgcolor="red">
</td>    
<td width="5%">
</td>
<td width="5%"><!--HIST-->
</td>
<?php }
else
{ ?>
                            <td class="align_right" width="10%">
                                <?php
                                if ($show) {
                                    echo isset($estacion['EA.Nivel']) ?
                                            round_number($estacion['EA.Nivel']) : "";
                                }
                                ?>
                            </td>            
                            <td class="align_right" width="10%">
                                <?php
                                if ($show) {
                                    echo isset($estacion['EA.Caudal']) ?
                                            round_number($estacion['EA.Caudal']) : "";
                                }
                                ?>
                            </td>    
                            <td width="5%">
                                <?php
                                $station = isset($estacion['label']) ? trim($estacion['label']) : "";
                                $show_graph_week = check_date_week($estacion['Ultima_com']);
                                if ($show AND $show_graph_week) {
									if ($station == 'CR2-53' OR $station == 'CR2-54') {
										?>
										<a href="./charts/charts_tr_nr.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
										   target="_blank" >
											TR
										</a>										
									<?php }
									else {?>
										<a href="./charts/charts_tr_cr.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
										   target="_blank" >
											TR
										</a>
                                <?php };
								}; ?>
                            </td>

                            <td width="5%"><!--HIST-->
                                <?php
								if (($show OR $show_graph_week) AND $show_historical['cr']) {
									if ($station == 'CR2-53' OR $station == 'CR2-54') { ?>
                                    <a href="./charts/charts_his_nr.php?station=<?php echo $station ?>&parameter=<?php echo $parameter; ?>&label=<?php echo $estacion['descripcion']; ?>"									
                                       target="_blank" >
                                        HIST
                                    </a>
									<?php } 
									else {?>
                                    <a href="./charts/charts_his_cr.php?station=<?php echo $station ?>&label=<?php echo $estacion['descripcion']; ?>" 
                                       target="_blank" >
                                        HIST
                                    </a>									
									<?php };
								}; ?>								
                            </td> <!--FIN HIST-->
<?php }
?> <!--FIN no es CR1-01-->
                            <td width="10%"><!--ALARMAS-->
                                <?php if ($show_alarms['cr']) { ?>
                                    <a href="./alarmas/?cod_estacion=<?php echo $station ?>&tipo_estacion=CR&descripcion=<?php echo $estacion['descripcion']; ?>"
                                       target="_blank" >
                                        Alarmas
                                    </a>                                        
                                <?php } ?>
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>