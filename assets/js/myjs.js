
function info($id, info, success, errors){   
    var $info =   $($id);
    $info.html(info);
    $info.stop().fadeIn(300).delay(5000).fadeOut(500);  // paramos efectos y seguidamente iniciamos fade => In - Out
    $info.removeClass('alert-error alert-info');   // First remove colors!
    if (success === false){
        $info.addClass('alert-error');       
        if (errors){
            $.each( errors, function( key, value ) {
                if (value.empty ===1){
                    $('input[name="'+value.name+'"]').css("border-color", "#ccc");    
                }else{
                    $('input[name="'+value.name+'"]').css("border-color", "#cc0000");    
                }         
            });
        }
        if (errors){
            setTimeout(
                function(){
                    $.each( errors, function( key, value ) {
                        $('input[name="'+value.name+'"]').css("border-color", "#ccc");    
                    });
                },3000
                ); 
        }  
    }else{
        $info.addClass('alert-info');
    }
}

function toggle_button($this){
    var $id  =$this.attr('id');
    var $button= $('#button_'+$id);
    if ($button.is(':disabled') == false) { 
        $button.attr('disabled', 'disabled');      
    }else{
        setTimeout(
            function(){
                $button.removeAttr('disabled');             
            },500
            ); 
    }
    return true;
}
function get_modal($this, $size){
    $this.dialog({
        autoOpen: false,
        height: $size.height,
        width: $size.width,
        modal: true,
        resizable: false
    });
    return $this;
}
function ajax_post_modal($this, $modal){
    //var data =  $this.serialize();   
    var options = {      
        //  beforeSubmit:  showRequest,  // pre-submit callback 
        success:       showResponse,  // post-submit callback       
        data: {
            modal: $modal           
        }
    }; 
     
    toggle_button($this);
    $($this).ajaxSubmit(options); 
    toggle_button($this);
    return false; 
}
 
// post-submit callback 
function showResponse(responseText, statusText, xhr, $form)  {
    var  data =  $.parseJSON(responseText);    
    info('.div_informacio_modal',data.info, data.success,data.errors);
   
    if (data.success === true){       
        setTimeout(
            function(){
                $( data.modal ).dialog( "close" );
                $('#'+data.table_id+ ' tr:last').after(data.row);
            },1500
            ); 
        setTimeout(
            function(){
                window.location.href= data.redirect;
            },2000
            ); 
    }        

} 

/*END FUNCIONALITIES*/
$(document).ready(function() {       
    $('#combo_tipos').change(function () {                
        var $this = $(this);
        var $selected = $this.val();
        $("html, body").animate({
            scrollTop: $('#tabla_'+$selected).offset().top -110
        }, 1000);
    });
    var $seconds = 60*15*1000; // 15 minutos     
    setTimeout(function(){
        window.location.reload(1);
    }, $seconds);
    
    var eventTime= moment().add('minutes', 15).unix();
    // Timestamp - Sun, 21 Apr 2013 13:00:00 GMT
    var currentTime = moment().unix(); // Timestamp - Sun, 21 Apr 2013 12:30:00 GMT
    var diffTime = eventTime - currentTime;
    var duration = moment.duration(diffTime*1000, 'milliseconds');
    var interval = 1000;

    setInterval(function(){
        duration = moment.duration(duration - interval, 'milliseconds');        
        $('#date').html(duration.hours() + ":" + duration.minutes() + ":" + duration.seconds())
    }, interval);    
        
    $('body').on("submit", ".form_ajax ", function(event){
        event.preventDefault();
        var $this = $(this);
        ajax_post_modal($this,"#modal_editar_alarma");
    });        
        
    $('body').on("click",".editar_alarma", function(event){
        event.preventDefault();
        var $this = $(this);
        var $size = {
            height: 550, 
            width:500
        }
        var $modal =  get_modal($("#modal_editar_alarma"), $size); 
        var $url = $this.data('url');
        var $cod_variable = $this.data('id');
        var $indice_alarma= $this.data('indice');
        var $tipo= $this.data('tipo');
        var $usuario= $this.data('usuario');

        $modal.empty();
        $modal.load($url, 
        {
            cod_variable: $cod_variable,
            indice_alarma: $indice_alarma,
            tipo: $tipo,
            usuario: $usuario
        },function() {        }).dialog('open'); 
    });
    
    $('body').on("click",".nueva_alarma", function(event){
        event.preventDefault();
        var $this = $(this);
        var $size = {
            height: 550, 
            width:500
        }
        var $modal =  get_modal($("#modal_nueva_alarma"), $size); 
        var $url = $this.data('url');
        var $cod_estacion = $this.data('estacion');
        var $tipo_estacion = $this.data('tipo');
        var $usuario = $this.data('usuario');

        $modal.empty();
        $modal.load($url, 
        {
            cod_estacion: $cod_estacion,
            tipo_estacion: $tipo_estacion,
            usuario: $usuario
        },function() {  }).dialog('open'); 
    });
 
    
    $('body').on("click",".borrar_alarma", function(event){
        event.preventDefault();
        var $this = $(this);
        var $url = $this.data('url');
        var $cod_variable = $this.data('id');
        var $indice_alarma= $this.data('indice');
        var $tipo= $this.data('tipo');

        var r=confirm("¿Está seguro que desea eliminar la alrma?");
        if (r==true)
        {
            $.get( $url, { 
                cod_variable: $cod_variable,
                indice_alarma: $indice_alarma,
                tipo: $tipo    
            } , function( data ) {
                data =  $.parseJSON(data);   
                info('.div_informacio',data.info, data.success,data.errors);
                if (data.success){
                    $this.parent().parent().fadeOut(600, function(){
                        $this.parent().parent().remove();
                    });
                }
            });

        }      
    });
//    var $popover =  $(".popover");
//    $popover.popover({       
//        placement: 'top', 
//        trigger: 'hover'
//    });
 
});    
   


